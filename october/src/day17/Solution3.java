package day17;

/**
 * 如果有大量输入的 S，称作 S1, S2, ... , Sk 其中 k >= 10亿，你需要依次检查它们是否为 T 的子序列。在这种情况下，你会怎样改变代码？
 *
 * 致谢：
 *
 * 特别感谢 @pbrother添加此问题并且创建所有测试用例。
 *
 *
 *
 * 示例 1：
 *
 * 输入：s = "abc", t = "ahbgdc"
 * 输出：true
 * 示例 2：
 *
 * 输入：s = "axc", t = "ahbgdc"
 * 输出：false
 *
 *
 */
public class Solution3 {
    public boolean isSubsequence(String s, String t) {
        int n = s.length(),m = t.length();
        int in=0,jm=0;
        while (in<n&&jm<m){
            if(s.charAt(in)==t.charAt(jm)){
                in++;
            }
            jm++;
        }
        return in==n;
    }
}
