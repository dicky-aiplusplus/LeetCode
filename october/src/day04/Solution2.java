package day04;

import java.util.HashMap;
import java.util.Map;

/**
 * 给你两个字符串：ransomNote 和 magazine ，判断 ransomNote 能不能由 magazine 里面的字符构成。
 *
 * 如果可以，返回 true ；否则返回 false 。
 *
 * magazine 中的每个字符只能在 ransomNote 中使用一次。
 *
 *
 *
 * 示例 1：
 *
 * 输入：ransomNote = "a", magazine = "b"
 * 输出：false
 * 示例 2：
 *
 * 输入：ransomNote = "aa", magazine = "ab"
 * 输出：false
 * 示例 3：
 *
 * 输入：ransomNote = "aa", magazine = "aab"
 * 输出：true
 *
 *
 */
public class Solution2 {
    public boolean canConstruct(String ransomNote, String magazine) {
        int n = ransomNote.length();
        int m = magazine.length();
        Map<Character,Integer> hm = new HashMap<>();
        for(int i = 0 ; i < m ;i++){
            hm.put(magazine.charAt(i),hm.getOrDefault(magazine.charAt(i),0)+1);
        }
        for(int i = 0; i < n ; i++){
            if(hm.containsKey(ransomNote.charAt(i))){
                hm.put(ransomNote.charAt(i),hm.get(ransomNote.charAt(i))-1);
            }else if (!hm.containsKey(ransomNote.charAt(i))){
                return false;
            }
            if(hm.get(ransomNote.charAt(i))==0){
                hm.remove(ransomNote.charAt(i));
            }
        }
        return true;
    }
}
