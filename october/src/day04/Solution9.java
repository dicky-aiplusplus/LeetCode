package day04;

import java.util.HashSet;
import java.util.Set;

/**
 * 给你字符串 s 和整数 k 。
 *
 * 请返回字符串 s 中长度为 k 的单个子字符串中可能包含的最大元音字母数。
 *
 * 英文中的 元音字母 为（a, e, i, o, u）。
 *
 *
 *
 * 示例 1：
 *
 * 输入：s = "abciiidef", k = 3
 * 输出：3
 * 解释：子字符串 "iii" 包含 3 个元音字母。
 * 示例 2：
 *
 * 输入：s = "aeiou", k = 2
 * 输出：2
 * 解释：任意长度为 2 的子字符串都包含 2 个元音字母。
 * 示例 3：
 *
 * 输入：s = "leetcode", k = 3
 * 输出：2
 * 解释："lee"、"eet" 和 "ode" 都包含 2 个元音字母。
 * 示例 4：
 *
 * 输入：s = "rhythms", k = 4
 * 输出：0
 * 解释：字符串 s 中不含任何元音字母。
 * 示例 5：
 *
 * 输入：s = "tryhard", k = 4
 * 输出：1
 *
 *
 */
//滑动窗口
public class Solution9 {
    public int maxVowels(String s, int k) {
        int n = s.length();
        int vowelNum=0;
        for (int i = 0; i < k; i++) {
            vowelNum+=isVowel(s.charAt(i));
        }
        int vowelCopy = vowelNum;
        for (int i = k; i < n; i++) {
            vowelCopy=vowelCopy-isVowel(s.charAt(i-k))+isVowel(s.charAt(i));
            vowelNum = Math.max(vowelCopy,vowelNum);
        }
        return vowelNum;
    }
    //写一个方法判断是不是是就返回1
    public int isVowel(char ch){
        return ch=='a'||ch=='e'||ch=='i'||ch=='o'||ch=='u'?1:0;
    }
}
