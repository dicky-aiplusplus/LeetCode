package day04;

import java.util.HashMap;
import java.util.Map;

/**
 * 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
 *
 * 注意：若s 和 t中每个字符出现的次数都相同，则称s 和 t互为字母异位词。
 *
 *
 *
 * 示例1:
 *
 * 输入: s = "anagram", t = "nagaram"
 * 输出: true
 * 示例 2:
 *
 * 输入: s = "rat", t = "car"
 * 输出: false
 *
 *
 */
public class Solution3 {
    public boolean isAnagram(String s, String t) {
        int sn = s.length();
        int tn = t.length();
        if(sn!=tn){
            return false;
        }
        Map<Character,Integer> hm = new HashMap<>();
        for(int i = 0 ; i < sn ; i++){
            hm.put(s.charAt(i),hm.getOrDefault(s.charAt(i),0)+1);
        }
        for (int i = 0;i<tn;i++){
            if(hm.containsKey(t.charAt(i))){
                hm.put(t.charAt(i),hm.get(t.charAt(i))-1);
            }else if (!hm.containsKey(t.charAt(i))){
                return false;
            }
            if(hm.get(t.charAt(i))==0){
                hm.remove(t.charAt(i));
            }
        }
        return hm.size()==0?true:false;
    }
}
