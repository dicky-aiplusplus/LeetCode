package day29;

import java.util.HashMap;
import java.util.Map;

/**
 * 给定两个字符串s和t，判断它们是否是同构的。
 *
 * 如果s中的字符可以按某种映射关系替换得到t，那么这两个字符串是同构的。
 *
 * 每个出现的字符都应当映射到另一个字符，同时不改变字符的顺序。不同字符不能映射到同一个字符上，相同字符只能映射到同一个字符上，字符可以映射到自己本身。
 *
 *
 *
 * 示例 1:
 *
 * 输入：s = "egg", t = "add"
 * 输出：true
 * 示例 2：
 *
 * 输入：s = "foo", t = "bar"
 * 输出：false
 * 示例 3：
 *
 * 输入：s = "paper", t = "title"
 * 输出：true
 *
 *
 */
public class Solution3 {
    public boolean isIsomorphic(String s, String t) {
        Map<Character,Character> hm = new HashMap<>();
        boolean []chars= new boolean[128];
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char charS = s.charAt(i);
            char charT = t.charAt(i);
            if(!hm.containsKey(charS)){
                if(chars[charT]){
                    return false;
                }
                chars[charT]=true;
                hm.put(charS,charT);
            }else {
                if(hm.get(charS)!=charT){
                    return false;
                }
            }
        }
        return true;
    }
}
