package day12;

/**
 * 给你两个数 hour 和 minutes 。请你返回在时钟上，由给定时间的时针和分针组成的较小角的角度（60 单位制）。
 *
 *
 *
 * 示例 1：
 *
 *
 *
 * 输入：hour = 12, minutes = 30
 * 输出：165
 * 示例 2：
 *
 *
 *
 * 输入：hour = 3, minutes = 30
 * 输出；75
 * 示例 3：
 *
 *
 *
 * 输入：hour = 3, minutes = 15
 * 输出：7.5
 * 示例 4：
 *
 * 输入：hour = 4, minutes = 50
 * 输出：155
 * 示例 5：
 *
 * 输入：hour = 12, minutes = 0
 * 输出：0
 */
public class Solution3 {
    public double angleClock(int hour, int minutes) {
        if(hour==12){
            hour=0;
        }
        double a = 30*hour+minutes*0.5;
        double b = minutes*6;
        if(b>a){
            return (360-b+a)>Math.abs(a-b)?Math.abs(a-b):(360-b+a);
        }else if(b<a){
            return (360-a+b)>Math.abs(a-b)?Math.abs(a-b):(360-a+b);
        }
        return 0;
    }
}
