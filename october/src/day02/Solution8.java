package day02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}

public class Solution8 {
    List<Integer> list = new ArrayList<>();
    public ListNode mergeKLists(ListNode[] lists) {
        for (ListNode listNode : lists) {
            add(listNode);
        }
        ListNode listNode = new ListNode();
        Collections.sort(list);

        return bdd(listNode,0);
    }
    public void add(ListNode listNode){
        if(listNode == null){
            return;
        }
        list.add(listNode.val);
        add(listNode.next);
    }
    public ListNode bdd(ListNode listNode,int i){
        if(i>=list.size()){
            return null;
        }
        listNode.val=list.get(i);
        i++;
        listNode.next=bdd(new ListNode(),i);
        return listNode;
    }
}
