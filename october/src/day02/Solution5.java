package day02;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

/**
 * 给你一个字符串表达式 s ，请你实现一个基本计算器来计算并返回它的值。
 * <p>
 * 注意:不允许使用任何将字符串作为数学表达式计算的内置函数，比如 eval() 。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：s = "1 + 1"
 * 输出：2
 * 示例 2：
 * <p>
 * 输入：s = " 2-1 + 2 "
 * 输出：3
 * 示例 3：
 * <p>
 * 输入：s = "(1+(4+5+2)-3)+(6+8)"
 * 输出：23
 */
public class Solution5 {
    public int calculate(String s) {
        Deque<Integer> stack = new LinkedList<>();
        stack.push(1);
        int mark = 1;
        int result = 0;
        int i = 0;
        while (i < s.length()) {
            if (s.charAt(i) == '(') {
                stack.push(mark);
                i++;
            } else if (s.charAt(i) == ')') {
                stack.pop();
                i++;
            } else if (s.charAt(i) == '+') {
                mark = stack.peek();
                i++;
            } else if (s.charAt(i) == '-') {
                mark = -stack.peek();
                i++;
            } else if (s.charAt(i) == ' ') {
                i++;
            } else {
                int n = 0;
                while (i < s.length() && Character.isDigit(s.charAt(i))) {
                    n = n * 10 + s.charAt(i) - '0';
                    i++;
                }
                result += mark * n;
            }
        }
        return result;
    }
}
