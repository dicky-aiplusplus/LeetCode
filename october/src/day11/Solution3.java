package day11;

/**
 * 给定二叉搜索树（BST）的根节点root和一个整数值val。
 *
 * 你需要在 BST 中找到节点值等于val的节点。 返回以该节点为根的子树。 如果节点不存在，则返回null。
 *
 *
 *
 * 示例 1:
 *
 *
 *
 * 输入：root = [4,2,7,1,3], val = 2
 * 输出：[2,1,3]
 * 示例 2:
 *
 *
 * 输入：root = [4,2,7,1,3], val = 5
 * 输出：[]
 *
 *
 */
public class Solution3 {
    public TreeNode searchBST(TreeNode root, int val) {
        return dfs(root,val);
    }
    public TreeNode dfs(TreeNode root,int t){
        if(root == null){
            return null;
        }
        if(root.val>t){
            return dfs(root.left,t);
        }else if(root.val<t){
            return dfs(root.right,t);
        }
        return root;
    }
}
