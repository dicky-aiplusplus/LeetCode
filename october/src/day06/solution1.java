package day06;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * 给定两个大小相等的数组nums1和nums2，nums1相对于 nums的优势可以用满足nums1[i] > nums2[i]的索引 i的数目来描述。
 *
 * 返回 nums1的任意排列，使其相对于 nums2的优势最大化。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums1 = [2,7,11,15], nums2 = [1,10,4,11]
 * 输出：[2,11,7,15]
 * 示例 2：
 *
 * 输入：nums1 = [12,24,8,32], nums2 = [13,25,32,11]
 * 输出：[24,32,8,12]
 *
 *
 */
public class solution1 {
    public int[] advantageCount(int[] nums1, int[] nums2) {
        int n = nums1.length;
        int []ans = new int[n];
        Arrays.sort(nums1);
        Integer[]ids = IntStream.range(0, n).boxed().toArray(Integer[]::new);
        Arrays.sort(ids, (i, j) -> nums2[i] - nums2[j]);
        int left = 0, right = n - 1;
        for (int x : nums1)
            ans[x > nums2[ids[left]] ? ids[left++] : ids[right--]] = x;
        return ans;
    }
}
