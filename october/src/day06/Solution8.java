package day06;

/**
 * 给定一个二进制数组nums和一个整数 k，如果可以翻转最多 k 个 0 ，则返回 数组中连续 1 的最大个数 。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums = [1,1,1,0,0,0,1,1,1,1,0], K = 2
 * 输出：6
 * 解释：[1,1,1,0,0,1,1,1,1,1,1]
 * 粗体数字从 0 翻转到 1，最长的子数组长度为 6。
 * 示例 2：
 *
 * 输入：nums = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], K = 3
 * 输出：10
 * 解释：[0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
 * 粗体数字从 0 翻转到 1，最长的子数组长度为 10。
 *
 *
 */
public class Solution8 {
    public int longestOnes(int[] nums, int k) {
        int right = 0,left = 0,cur = 0,n=nums.length,ans=0;
        while (right<n){
            while (right<n&&cur<=k){
                if(nums[right]==0){
                    cur++;
                }
                right++;
                if(cur<=k){
                    ans=Math.max(ans,right-left);
                }
            }
            while (left<=right&&cur>k){
                if(nums[left]==0){
                    cur--;
                }
                left++;
            }
        }
        return ans;
    }
}
