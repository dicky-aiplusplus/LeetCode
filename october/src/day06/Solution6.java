package day06;

/**
 * 给你两个字符串s1和s2 ，写一个函数来判断 s2 是否包含 s1的排列。如果是，返回 true ；否则，返回 false 。
 * <p>
 * 换句话说，s1 的排列之一是 s2 的 子串 。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：s1 = "ab" s2 = "eidbaooo"
 * 输出：true
 * 解释：s2 包含 s1 的排列之一 ("ba").
 * 示例 2：
 * <p>
 * 输入：s1= "ab" s2 = "eidboaoo"
 * 输出：false
 */
public class Solution6 {
    public boolean checkInclusion(String s1, String s2) {
        int len1 = s1.length(), len2 = s2.length();
        int right = 0, left = 0;
        int[] charT = new int[26];
        for (int i = 0; i < len1; i++) {
            charT[s1.charAt(i) - 'a']++;
        }
        while (right < len2) {
            charT[s2.charAt(right) - 'a']--;

            while (charT[s2.charAt(right) - 'a'] < 0) {

                charT[s2.charAt(left) - 'a']++;
                left++;
            }
            if (right - left + 1 == len1) {
                return true;
            }
            right++;
        }
        return false;
    }
}
