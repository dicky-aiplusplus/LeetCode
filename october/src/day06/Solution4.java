package day06;

import java.util.HashMap;
import java.util.Map;

/**
 * 给你一个正整数数组 nums ，请你从中删除一个含有 若干不同元素 的子数组。删除子数组的 得分 就是子数组各元素之 和 。
 *
 * 返回 只删除一个 子数组可获得的 最大得分 。
 *
 * 如果数组 b 是数组 a 的一个连续子序列，即如果它等于 a[l],a[l+1],...,a[r] ，那么它就是a 的一个子数组。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums = [4,2,4,5,6]
 * 输出：17
 * 解释：最优子数组是 [2,4,5,6]
 * 示例 2：
 *
 * 输入：nums = [5,2,1,2,5,2,1,2,5]
 * 输出：8
 * 解释：最优子数组是 [5,2,1] 或 [1,2,5]
 *
 *
 */
public class Solution4 {
    public int maximumUniqueSubarray(int[] nums) {
        int right =0 ,left = 0, len = nums.length;
        Map<Integer,Integer> hm = new HashMap<>();
        int sum=0;
        int max=0;
        while (right<len){
            int value = nums[right];
            hm.put(value,hm.getOrDefault(value,0)+1);
            sum+=value;
            right++;
            while(hm.get(value) != 1){
                hm.put(nums[left],hm.getOrDefault(nums[left],0)-1);
                sum -= nums[left];
                left++;
            }
            max = Math.max(max,sum);
        }
        return max;
    }
}
