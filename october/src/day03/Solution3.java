package day03;

import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class Solution3 {
    public void setZeroes(int[][] matrix) {
        Set<Integer> w = new HashSet<>();
        Set<Integer> l = new HashSet<>();
        for(int i = 0;i<matrix.length;i++){
            for(int j = 0;j<matrix[0].length;j++){
                if(matrix[i][j]==0){
                    w.add(i);
                    l.add(j);
                }
            }
        }
        for (Integer integer : w) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[integer][j]!=0) {
                    matrix[integer][j]=0;
                }
            }
        }
        for (Integer integer : l) {
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i][integer]!=0) {
                    matrix[i][integer]=0;
                }
            }
        }
    }
}
