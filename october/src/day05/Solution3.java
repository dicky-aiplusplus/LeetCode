package day05;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 
 *
 *  
 *
 * 示例 1：
 *
 *
 * 输入：l1 = [1,2,4], l2 = [1,3,4]
 * 输出：[1,1,2,3,4,4]
 * 示例 2：
 *
 * 输入：l1 = [], l2 = []
 * 输出：[]
 * 示例 3：
 *
 * 输入：l1 = [], l2 = [0]
 * 输出：[0]
 *
 *
 */
class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }
public class Solution3 {
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if(list1==null&&list2==null){
            return null;
        }
        if(list1==null||list2==null){
            return list1==null?list2:list1;
        }
        List<Integer> list = new ArrayList<>();
        ListNode node = list1;
        while (node!=null){
            list.add(node.val);
            node=node.next;
        }
        node=list2;
        while (node!=null){
            list.add(node.val);
            node=node.next;
        }
        Collections.sort(list);
        return reconstitution(new ListNode(),0,list);

    }
    public ListNode reconstitution(ListNode node , int i,List<Integer> list){
        if(i>=list.size()){
            return null;
        }
        node.val= list.get(i);
        node.next=reconstitution(new ListNode(),i+1,list);
        return node;
    }
}
