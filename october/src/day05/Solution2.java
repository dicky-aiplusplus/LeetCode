package day05;

public class Solution2 {
    public int lengthOfLongestSubstring(String s) {
        int len = s.length();
        if(len<2){
            return len;
        }
        int []charS= new int[128];
        int right = 0 ;
        int left = 0;
        int maxCount = 0;
        while(right<len){
            charS[s.charAt(right)-'\0']++;
            right++;
            if(right<len&&charS[s.charAt(right)-'\0']==1){
                maxCount=Math.max(maxCount,right-left);
                while(charS[s.charAt(right)-'\0']==1){
                    charS[s.charAt(left)-'\0']--;
                    left++;
                }
            }
        }
        return Math.max(maxCount,right-left);
    }
}
