package day05;

/**
 * 给定一个含有n个正整数的数组和一个正整数 target 。
 * <p>
 * 找出该数组中满足其和 ≥ target 的长度最小的 连续子数组[numsl, numsl+1, ..., numsr-1, numsr] ，并返回其长度。如果不存在符合条件的子数组，返回 0 。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：target = 7, nums = [2,3,1,2,4,3]
 * 输出：2
 * 解释：子数组[4,3]是该条件下的长度最小的子数组。
 * 示例 2：
 * <p>
 * 输入：target = 4, nums = [1,4,4]
 * 输出：1
 * 示例 3：
 * <p>
 * 输入：target = 11, nums = [1,1,1,1,1,1,1,1]
 * 输出：0
 */
public class Solution5 {
    public int minSubArrayLen(int target, int[] nums) {
        int len = nums.length;
        int right = 0;
        int left = 0;
        int sum = 0;
        int minLen = 0;
        while (right < len) {
            sum += nums[right];
            right++;
            if (sum >= target) {
                while (sum >= target) {
                    sum -= nums[left];
                    left++;
                }
                if (minLen == 0) {
                    minLen = right - left;
                } else {
                    minLen = Math.min(right - left, minLen);
                }
            }
        }
        return minLen;
    }
}
