package day05;

/**
 * 给你一个字符串 s 和一个整数 k 。你可以选择字符串中的任一字符，并将其更改为任何其他大写英文字符。该操作最多可执行 k 次。
 *
 * 在执行上述操作后，返回包含相同字母的最长子字符串的长度。
 *
 *
 *
 * 示例 1：
 *
 * 输入：s = "ABAB", k = 2
 * 输出：4
 * 解释：用两个'A'替换为两个'B',反之亦然。
 * 示例 2：
 *
 * 输入：s = "AABABBA", k = 1
 * 输出：4
 * 解释：
 * 将中间的一个'A'替换为'B',字符串变为 "AABBBBA"。
 * 子串 "BBBB" 有最长重复字母, 答案为 4。
 *
 *
 */
public class Solution1 {
    public int characterReplacement(String s, int k) {
        int n = s.length();
        if(n<2){
            return n;
        }
        int left = 0;
        int right = 0;
        int maxCount = 0;
        int []charA=new int[26];
        while(right<n){
            charA[s.charAt(right)-'A']++;
            maxCount=Math.max(maxCount,charA[s.charAt(right)-'A']);
            right++;
            if(right-left>maxCount+k){
                charA[s.charAt(left)-'A']--;
                left++;
            }
        }
        return right-left;
    }
}
