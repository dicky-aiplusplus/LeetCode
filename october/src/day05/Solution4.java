package day05;

import java.util.ArrayList;
import java.util.List;

/**
 * 给你一个链表的头节点 head 和一个整数 val ，请你删除链表中所有满足 Node.val == val 的节点，并返回 新的头节点 。
 *
 *
 * 示例 1：
 *
 *
 * 输入：head = [1,2,6,3,4,5,6], val = 6
 * 输出：[1,2,3,4,5]
 * 示例 2：
 *
 * 输入：head = [], val = 1
 * 输出：[]
 * 示例 3：
 *
 * 输入：head = [7,7,7,7], val = 7
 * 输出：[]
 *
 *
 */
public class Solution4 {
    public ListNode removeElements(ListNode head, int val) {
        if(head==null){
            return null;
        }
        ListNode node = head;
        List<Integer> list = new ArrayList<>();
        while (node!=null){
            if(node.val!=val){
                list.add(node.val);
            }
            node=node.next;
        }
        return reconstitution(new ListNode(),0,list);
    }
    public ListNode reconstitution(ListNode node , int i,List<Integer> list){
        if(i>=list.size()){
            return null;
        }
        node.val= list.get(i);
        node.next=reconstitution(new ListNode(),i+1,list);
        return node;
    }
}
