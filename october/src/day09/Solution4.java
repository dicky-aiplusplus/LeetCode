package day09;

/**
 * 给你一个二叉树的根节点 root ， 检查它是否轴对称。
 */
public class Solution4 {
    public boolean isSymmetric(TreeNode root) {
        return dfs(root,root);
    }
    public boolean dfs(TreeNode node1,TreeNode node2){
        if(node1==null&&node2==null){
            return true;
        }
        if(node1==null||node2==null){
            return false;
        }
        return node1.val==node2.val&&dfs(node1.left,node2.right)&&dfs(node1.right,node2.left);
    }
}
