package day09;

public class Solution3 {
    int count = 0;
    public int maxDepth(TreeNode root) {
        dfs(root,0);
        return count;
    }
    public void dfs(TreeNode node,int i){
        if(node == null){
            return;
        }
        count = Math.max(count,i+1);
        dfs(node.left,i+1);
        dfs(node.right,i+1);
    }
}
