package day14;

import java.util.Arrays;

/**
 * 给定一个长度为 n 的整数数组和一个目标值 target，寻找能够使条件nums[i] + nums[j] + nums[k] < target成立的三元组 i, j, k个数（0 <= i < j < k < n）。
 *
 *
 *
 * 示例 1：
 *
 * 输入: nums = [-2,0,1,3], target = 2
 * 输出: 2
 * 解释: 因为一共有两个三元组满足累加和小于 2:
 *     [-2,0,1]
 *      [-2,0,3]
 * 示例 2：
 *
 * 输入: nums = [], target = 0
 * 输出: 0
 * 示例 3：
 *
 * 输入: nums = [0], target = 0
 * 输出: 0
 *
 *
 */
public class Solution7 {
    public int threeSumSmaller(int[] nums, int target) {
        int count = 0;
        Arrays.sort(nums);
        int n = nums.length;
        for (int i = 0; i < n - 2; i++) {
            int j = i + 1, k = n - 1;
            while (j < k) {
                int sum = nums[i] + nums[j] + nums[k];
                if (sum < target) {
                    count += k - j;
                    j++;
                } else {
                    k--;
                }
            }
        }
        return count;
    }
}
