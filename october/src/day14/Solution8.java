package day14;

import java.util.Arrays;

/**
 *
 给你一个已经排好序的整数数组nums和整数a、b、c。对于数组中的每一个元素nums[i]，计算函数值f(x) = ax2 + bx + c，请 按升序返回数组 。



 示例 1：

 输入: nums = [-4,-2,2,4], a = 1, b = 3, c = 5
 输出: [3,9,15,33]
 示例 2：

 输入: nums = [-4,-2,2,4], a = -1, b = 3, c = 5
 输出: [-23,-5,1,7]


 */
public class Solution8 {
    public int[] sortTransformedArray(int[] nums, int a, int b, int c) {
        int len = nums.length;
        int []ans = new int[len];
        for(int i = 0; i<len;i++){
            ans[i]=a*nums[i]*nums[i]+b*nums[i]+c;
        }
        Arrays.sort(ans);
        return ans;
    }
}
