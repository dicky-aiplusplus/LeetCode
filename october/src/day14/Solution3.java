package day14;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 给你一个字符串 s ，仅反转字符串中的所有元音字母，并返回结果字符串。
 *
 * 元音字母包括 'a'、'e'、'i'、'o'、'u'，且可能以大小写两种形式出现。
 *
 *
 *
 * 示例 1：
 *
 * 输入：s = "hello"
 * 输出："holle"
 * 示例 2：
 *
 * 输入：s = "leetcode"
 * 输出："leotcede"
 *
 *
 */
public class Solution3 {
    public String reverseVowels(String s) {
        char[] chars = s.toCharArray();
        Set<Character> hs = new HashSet<>();
        hs.add('a');
        hs.add('e');
        hs.add('i');
        hs.add('o');
        hs.add('u');
        hs.add('A');
        hs.add('E');
        hs.add('I');
        hs.add('O');
        hs.add('U');
        int end = s.length()-1;
        int start = 0;
        while (end>start){
            if(hs.contains(chars[start])&&hs.contains(chars[end])){
                char ch = chars[start];
                chars[start]=chars[end];
                chars[end]=ch;
                start++;
                end--;
            }else if(hs.contains(chars[start])&&!hs.contains(chars[end])){
                end--;
            }else if(!hs.contains(chars[start])&&hs.contains(chars[end])){
                start++;
            }else if(!hs.contains(chars[start])&&!hs.contains(chars[end])){
                start++;
                end--;
            }
        }
        return  String.valueOf(chars);
    }
}
