package day01;

/**
 * 给你一个大小为 m x n 的二进制矩阵 grid 。
 *
 * 岛屿是由一些相邻的1(代表土地) 构成的组合，这里的「相邻」要求两个 1 必须在 水平或者竖直的四个方向上 相邻。你可以假设grid 的四个边缘都被 0（代表水）包围着。
 *
 * 岛屿的面积是岛上值为 1 的单元格的数目。
 *
 * 计算并返回 grid 中最大的岛屿面积。如果没有岛屿，则返回面积为 0 。
 *
 *
 */
public class Solution3 {

    public int maxAreaOfIsland(int[][] grid) {
        int maxIsland = 0;
        int m = grid.length;
        int n = grid[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if(grid[i][j]==1){
                    int max = dfs(grid,i,j,m,n);
                    maxIsland=Math.max(max,maxIsland);
                }
            }
        }
        return maxIsland;
    }
    public int dfs(int[][] grid,int i,int j ,int m,int n){
        if(i<0||j<0||i>=m||j>=n||grid[i][j]==0){
            return 0;
        }
        int count =1;
        grid[i][j]=0;
        return count + dfs(grid,i-1,j,m,n) + dfs(grid,i+1,j,m,n)+dfs(grid,i,j-1,m,n)+dfs(grid,i,j+1,m,n);
    }
}
