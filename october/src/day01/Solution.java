package day01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 给你两个整数数组nums1 和 nums2 ，请你以数组形式返回两数组的交集。返回结果中每个元素出现的次数，应与元素在两个数组中都出现的次数一致（如果出现次数不一致，则考虑取较小值）。可以不考虑输出结果的顺序。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums1 = [1,2,2,1], nums2 = [2,2]
 * 输出：[2,2]
 * 示例 2:
 *
 * 输入：nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * 输出：[4,9]
 *
 *
 */
public class Solution {
    public int[] intersect(int[] nums1, int[] nums2) {
        int m = nums1.length;
        int n = nums2.length;
        int []arr = new int[Math.min(m,n)];
        int index=0;
        int index1=0;
        int index2=0;
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        while (index1<m&&index2<n){
            if(nums1[index1]==nums2[index2]){
                arr[index]=nums1[index1];
                index++;
                index1++;
                index2++;
            }else if(nums1[index1]<nums2[index2]){
                index1++;
            }else {
                index2++;
            }
        }
        return Arrays.copyOfRange(arr,0,index);
    }
}
