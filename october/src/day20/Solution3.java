package day20;

/**
 * 当且仅当每个相邻位数上的数字x和y满足x <= y时，我们称这个整数是单调递增的。
 *
 * 给定一个整数 n ，返回 小于或等于 n 的最大数字，且数字呈 单调递增 。
 *
 *
 *
 * 示例 1:
 *
 * 输入: n = 10
 * 输出: 9
 * 示例 2:
 *
 * 输入: n = 1234
 * 输出: 1234
 * 示例 3:
 *
 * 输入: n = 332
 * 输出: 299
 *
 *
 */
public class Solution3 {
    public int monotoneIncreasingDigits(int n) {
        if(n<10){
            return n;
        }
        String sn = ""+n;
        char[] chars = sn.toCharArray();
        int i = 0;
        while (i<chars.length-1){
            if(chars[i]>chars[i+1]){
                char chr = chars[i];
                while (i>=0&&chars[i]==chr){
                    i--;
                }
                i++;
                chars[i]--;
                i++;
                while (i<chars.length){
                    chars[i]='9';
                    i++;
                }
            }
            i++;
        }
        boolean f = true;
        StringBuilder str = new StringBuilder();
        for (int j = 0; j < chars.length; j++) {
            if(chars[j]=='0'&&f){
                continue;
            }
            if(chars[j]!='0'){
                f=false;
                str.append(chars[j]);
            }
        }
        return Integer.parseInt(str.toString());
    }
}
