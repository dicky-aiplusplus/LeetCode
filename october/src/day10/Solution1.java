package day10;

import java.util.HashSet;
import java.util.Set;

/**
 * 给定链表头结点head，该链表上的每个结点都有一个 唯一的整型值 。同时给定列表nums，该列表是上述链表中整型值的一个子集。
 *
 * 返回列表nums中组件的个数，这里对组件的定义为：链表中一段最长连续结点的值（该值必须在列表nums中）构成的集合。
 *
 *
 *
 * 示例1：
 *
 *
 *
 * 输入: head = [0,1,2,3], nums = [0,1,3]
 * 输出: 2
 * 解释: 链表中,0 和 1 是相连接的，且 nums 中不包含 2，所以 [0, 1] 是 nums 的一个组件，同理 [3] 也是一个组件，故返回 2。
 * 示例 2：
 *
 *
 *
 * 输入: head = [0,1,2,3,4], nums = [0,3,1,4]
 * 输出: 2
 * 解释: 链表中，0 和 1 是相连接的，3 和 4 是相连接的，所以 [0, 1] 和 [3, 4] 是两个组件，故返回 2。
 *
 *
 */
class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}

public class Solution1 {
    public int numComponents(ListNode head, int[] nums) {
        Set<Integer> hs= new HashSet<>();
        for (int num : nums) {
            hs.add(num);
        }
        int count = 0;
        boolean f = false;
        while (head!=null){
            if (hs.contains(head.val)){
                if(!f){
                    count++;
                    f=true;
                }
            }else {
                f=false;
            }
            head=head.next;
        }
        return count;
    }
}
