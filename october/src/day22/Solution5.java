package day22;

/**
 * 给你一个整数数组nums ，除某个元素仅出现 一次 外，其余每个元素都恰出现 三次 。请你找出并返回那个只出现了一次的元素。
 *
 * 你必须设计并实现线性时间复杂度的算法且不使用额外空间来解决此问题。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums = [2,2,3,2]
 * 输出：3
 * 示例 2：
 *
 * 输入：nums = [0,1,0,1,0,1,99]
 * 输出：99
 *
 *
 */
public class Solution5 {
    public int singleNumber(int[] nums) {
        int one=0,two=0;
        for (int num : nums) {
            //a&0=0,a&(~a)=0,a^a=0,a^0=a;
            one=one^num&(~two);
            two=two^num&(~one);
        }
        return one;
    }
}
