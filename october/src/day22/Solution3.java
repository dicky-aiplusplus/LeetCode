package day22;

/**
 * 给你一个整数 n ，对于0 <= i <= n 中的每个 i ，计算其二进制表示中 1 的个数 ，返回一个长度为 n + 1 的数组 ans 作为答案。
 *
 *
 *
 * 示例 1：
 *
 * 输入：n = 2
 * 输出：[0,1,1]
 * 解释：
 * 0 --> 0
 * 1 --> 1
 * 2 --> 10
 * 示例 2：
 *
 * 输入：n = 5
 * 输出：[0,1,1,2,1,2]
 * 解释：
 * 0 --> 0
 * 1 --> 1
 * 2 --> 10
 * 3 --> 11
 * 4 --> 100
 * 5 --> 101
 *
 *
 */
public class Solution3 {
    public int[] countBits(int n) {
        int []ans= new int[n+1];
        ans[0]=0;
        for (int i = 1; i < n + 1; i++) {
            int num = 0;
            int index = i;
            while (index!=0){
                index=index&(index-1);
                num++;
            }
            ans[i]=num;
        }
        return ans;
    }
}
