package day15;

import java.util.Arrays;

/**
 * 给你一个整数数组 nums 和整数 k ，返回最大和 sum ，满足存在 i < j 使得 nums[i] + nums[j] = sum 且 sum < k 。如果没有满足此等式的 i,j 存在，则返回 -1 。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums = [34,23,1,24,75,33,54,8], k = 60
 * 输出：58
 * 解释：
 * 34 和 24 相加得到 58，58 小于 60，满足题意。
 * 示例2：
 *
 * 输入：nums = [10,20,30], k = 15
 * 输出：-1
 * 解释：
 * 我们无法找到和小于 15 的两个元素。
 *
 *
 */
public class Solution4 {
    public int twoSumLessThanK(int[] nums, int k) {
        Arrays.sort(nums);
        int right =nums.length-1,left=0;
        int ans = -1;
        while (right>left){
            int sum = nums[right]+nums[left];
            if(sum>=k){
                right--;
            }
            if(sum<k){
                ans=Math.max(ans,sum);
                left++;
            }
        }
        return ans;
    }
}
