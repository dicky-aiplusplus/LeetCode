package day15;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 给定两个人的空闲时间表：slots1 和 slots2，以及会议的预计持续时间duration，请你为他们安排时间段最早且合适的会议时间。
 *
 * 如果没有满足要求的会议时间，就请返回一个 空数组。
 *
 * 「空闲时间」的格式是[start, end]，由开始时间start和结束时间end组成，表示从start开始，到 end结束。
 *
 * 题目保证数据有效：同一个人的空闲时间不会出现交叠的情况，也就是说，对于同一个人的两个空闲时间[start1, end1]和[start2, end2]，要么start1 > end2，要么start2 > end1。
 *
 *
 *
 * 示例 1：
 *
 * 输入：slots1 = [[10,50],[60,120],[140,210]], slots2 = [[0,15],[60,70]], duration = 8
 * 输出：[60,68]
 * 示例 2：
 *
 * 输入：slots1 = [[10,50],[60,120],[140,210]], slots2 = [[0,15],[60,70]], duration = 12
 * 输出：[]
 *
 *
 */
public class Solution5 {
    public List<Integer> minAvailableDuration(int[][] slots1, int[][] slots2, int duration) {
        List<Integer> res = new ArrayList<>();
        Arrays.sort(slots1, (o1, o2) -> (o1[0] - o2[0]));
        Arrays.sort(slots2, (o1, o2) -> (o1[0] - o2[0]));
        int left = 0;
        int right = 0;
        while (left < slots1.length && right < slots2.length) {
            int l, r;
            l = Math.max(slots1[left][0], slots2[right][0]);
            r = Math.min(slots1[left][1], slots2[right][1]);
            if (r - l >= duration) {
                res.add(l);
                res.add(l + duration);
                return res;
            }
            if (slots1[left][1] < slots2[right][1]) {
                left++;
            } else {
                right++;
            }
        }
        return res;
    }
}
