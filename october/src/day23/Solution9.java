package day23;

/**
 * 给定一个数组coordinates，其中coordinates[i] = [x, y]，[x, y]表示横坐标为 x、纵坐标为 y的点。请你来判断，这些点是否在该坐标系中属于同一条直线上。
 *
 *
 *
 * 示例 1：
 *
 *
 *
 * 输入：coordinates = [[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]]
 * 输出：true
 * 示例 2：
 *
 *
 *
 * 输入：coordinates = [[1,1],[2,2],[3,4],[4,5],[5,6],[7,7]]
 * 输出：false
 *
 *
 */
public class Solution9 {
    public boolean checkStraightLine(int[][] coordinates) {
        int length = coordinates.length;
        if (length == 2) {
            return true;
        }
        int deltaX = coordinates[1][0] - coordinates[0][0], deltaY = coordinates[1][1] - coordinates[0][1];
        for (int i = 2; i < length; i++) {
            int curDeltaX = coordinates[i][0] - coordinates[i - 1][0], curDeltaY = coordinates[i][1] - coordinates[i - 1][1];
            if (deltaX * curDeltaY != deltaY * curDeltaX) {
                return false;
            }
        }
        return true;
    }
}
