package day23;

import java.util.Arrays;

/**
 * 给定2D空间中四个点的坐标p1,p2,p3和p4，如果这四个点构成一个正方形，则返回 true 。
 *
 * 点的坐标pi 表示为 [xi, yi] 。 输入没有任何顺序 。
 *
 * 一个 有效的正方形 有四条等边和四个等角(90度角)。
 *
 *
 *
 * 示例 1:
 *
 * 输入: p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,1]
 * 输出: True
 * 示例 2:
 *
 * 输入：p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,12]
 * 输出：false
 * 示例 3:
 *
 * 输入：p1 = [1,0], p2 = [-1,0], p3 = [0,1], p4 = [0,-1]
 * 输出：true
 *
 *
 */
public class Solution10 {
    public boolean validSquare(int[] p1, int[] p2, int[] p3, int[] p4) {
        if (p1[0] == p2[0] && p1[1] == p2[1]) {
            return false;
        }
        if (p1[0] == p3[0] && p1[1] == p3[1]) {
            return false;
        }
        if (p1[0] == p4[0] && p1[1] == p4[1]) {
            return false;
        }
        if (p2[0] == p3[0] && p2[1] == p3[1]) {
            return false;
        }
        if (p2[0] == p4[0] && p2[1] == p4[1]) {
            return false;
        }
        if (p3[0] == p4[0] && p3[1] == p4[1]) {
            return false;
        }
        int[] distanceSquares = new int[6];
        distanceSquares[0] = distanceSquare(p1, p2);
        distanceSquares[1] = distanceSquare(p1, p3);
        distanceSquares[2] = distanceSquare(p1, p4);
        distanceSquares[3] = distanceSquare(p2, p3);
        distanceSquares[4] = distanceSquare(p2, p4);
        distanceSquares[5] = distanceSquare(p3, p4);
        Arrays.sort(distanceSquares);
        if (distanceSquares[0] != distanceSquares[1] || distanceSquares[1] != distanceSquares[2] || distanceSquares[2] != distanceSquares[3]) {
            return false;
        } else if (distanceSquares[4] != distanceSquares[5]) {
            return false;
        } else {
            return true;
        }
    }

    public int distanceSquare(int[] p1, int[] p2) {
        return (p2[0] - p1[0]) * (p2[0] - p1[0]) + (p2[1] - p1[1]) * (p2[1] - p1[1]);
    }
}
