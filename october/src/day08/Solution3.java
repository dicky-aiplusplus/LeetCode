package day08;

/**
 * 你的好友是一位健身爱好者。前段日子，他给自己制定了一份健身计划。现在想请你帮他评估一下这份计划是否合理。
 *
 * 他会有一份计划消耗的卡路里表，其中calories[i]给出了你的这位好友在第i天需要消耗的卡路里总量。
 *
 * 为了更好地评估这份计划，对于卡路里表中的每一天，你都需要计算他 「这一天以及之后的连续几天」 （共k 天）内消耗的总卡路里 T：
 *
 * 如果T < lower，那么这份计划相对糟糕，并失去 1 分；
 * 如果 T > upper，那么这份计划相对优秀，并获得 1 分；
 * 否则，这份计划普普通通，分值不做变动。
 * 请返回统计完所有calories.length天后得到的总分作为评估结果。
 *
 * 注意：总分可能是负数。
 *
 *
 *
 * 示例 1：
 *
 * 输入：calories = [1,2,3,4,5], k = 1, lower = 3, upper = 3
 * 输出：0
 * 解释：calories[0], calories[1] < lower 而 calories[3], calories[4] > upper, 总分 = 0.
 * 示例 2：
 *
 * 输入：calories = [3,2], k = 2, lower = 0, upper = 1
 * 输出：1
 * 解释：calories[0] + calories[1] > upper, 总分 = 1.
 * 示例 3：
 *
 * 输入：calories = [6,5,0,0], k = 2, lower = 1, upper = 5
 * 输出：0
 * 解释：calories[0] + calories[1] > upper, calories[2] + calories[3] < lower, 总分 = 0.
 *
 *
 */
public class Solution3 {
    public int dietPlanPerformance(int[] calories, int k, int lower, int upper) {
        int len = calories.length;
        int right = 0,left=0;
        int sum = 0 ,count =0;
        while (right<len){
            sum+=calories[right];
            if(right-left+1==k){
                if(sum<lower){
                    count--;
                }else if(sum>upper){
                    count++;
                }
                while (right<len-1){
                    right++;
                    sum=sum-calories[left]+calories[right];
                    left++;
                    if(sum<lower){
                        count--;
                    }else if(sum>upper){
                        count++;
                    }
                }
            }
            right++;
        }
        return count;
    }
}
