package day08;

/**
 * 给你一个字符串 s，它只包含三种字符 a, b 和 c 。
 *
 * 请你返回 a，b 和 c 都至少出现过一次的子字符串数目。
 *
 *
 *
 * 示例 1：
 *
 * 输入：s = "abcabc"
 * 输出：10
 * 解释：包含 a，b 和 c 各至少一次的子字符串为 "abc", "abca", "abcab", "abcabc", "bca", "bcab", "bcabc", "cab", "cabc" 和 "abc" (相同字符串算多次)。
 * 示例 2：
 *
 * 输入：s = "aaacb"
 * 输出：3
 * 解释：包含 a，b 和 c 各至少一次的子字符串为 "aaacb", "aacb" 和 "acb" 。
 * 示例 3：
 *
 * 输入：s = "abc"
 * 输出：1
 *
 *
 */
public class Solution10 {
    public int numberOfSubstrings(String s) {
        int ans = 0;
        int len = s.length();
        int []charT=new int[3];
        int left = 0,right =0;
        while(right<len){
            charT[s.charAt(right)-'a']++;
            while(charT[0]>=1&&charT[1]>=1&&charT[2]>=1){
                ans+=len-right;
                charT[s.charAt(left)-'a']--;
                left++;
            }
            right++;
        }
        return ans;
    }
}
