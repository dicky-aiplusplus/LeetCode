package day08;

/**
 *给你一个字符串 s ，请你找出至多包含 两个不同字符 的最长子串，并返回该子串的长度。
 *
 *
 * 示例 1：
 *
 * 输入：s = "eceba"
 * 输出：3
 * 解释：满足题目要求的子串是 "ece" ，长度为 3 。
 * 示例 2：
 *
 * 输入：s = "ccaabbb"
 * 输出：5
 * 解释：满足题目要求的子串是 "aabbb" ，长度为 5 。
 *
 *
 */
public class Solution4 {
    public int lengthOfLongestSubstringTwoDistinct(String s) {
        int len = s.length();
        if(len<3){
            return len;
        }
        char []chars=s.toCharArray();
        int []charT= new int[128];
        int count = 0;
        int right = 0;
        int left = 0;
        int ans = 2;
        while(right<len){
            charT[chars[right]]++;
            if(charT[chars[right]]==1){
                count++;
            }
            while(count == 3){
                charT[chars[left]]--;
                if(charT[chars[left]]==0){
                    count--;
                }
                left++;
            }
            ans = Math.max(ans,right-left+1);
            right++;
        }
        return ans;
    }
}
