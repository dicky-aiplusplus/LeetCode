package day08;

/**
 * 我们有两个长度相等且不为空的整型数组nums1和nums2。在一次操作中，我们可以交换nums1[i]和nums2[i]的元素。
 *
 * 例如，如果 nums1 = [1,2,3,8] ， nums2 =[5,6,7,4] ，你可以交换 i = 3 处的元素，得到 nums1 =[1,2,3,4] 和 nums2 =[5,6,7,8] 。
 * 返回 使 nums1 和 nums2 严格递增所需操作的最小次数 。
 *
 * 数组arr严格递增 且arr[0] < arr[1] < arr[2] < ... < arr[arr.length - 1]。
 *
 *
 */
public class Solution1 {
    public int minSwap(int[] nums1, int[] nums2) {
        int a=0,b=1;
        int len = nums1.length;
        for (int i = 1; i < len; i++) {
            int at = a,bt = b;
            a=b=len;
            if(nums1[i]>nums1[i-1]&&nums2[i]>nums2[i-1]){
                a=Math.min(a,at);
                b=Math.min(b,bt+1);
            }
            if(nums1[i-1]<nums2[i]&&nums2[i-1]<nums1[i]){
                a=Math.min(a,bt);
                b=Math.min(b,at+1);
            }
        }
        return Math.min(a,b);
    }
}
