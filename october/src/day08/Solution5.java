package day08;

/**
 *给你一个字符串 s 和一个整数 k ，请你找出至多包含 k 个 不同 字符的最长子串，并返回该子串的长度。
 *
 *
 *
 * 示例 1：
 *
 * 输入：s = "eceba", k = 2
 * 输出：3
 * 解释：满足题目要求的子串是 "ece" ，长度为 3 。
 * 示例 2：
 *
 * 输入：s = "aa", k = 1
 * 输出：2
 * 解释：满足题目要求的子串是 "aa" ，长度为 2 。
 *
 *
 */
public class Solution5 {
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        int len = s.length();
        if(len<k+1){
            return len;
        }
        char []chars=s.toCharArray();
        int []charT= new int[128];
        int count = 0;
        int right = 0;
        int left = 0;
        int ans = k;
        while(right<len){
            charT[chars[right]]++;
            if(charT[chars[right]]==1){
                count++;
            }
            while(count == k+1){
                charT[chars[left]]--;
                if(charT[chars[left]]==0){
                    count--;
                }
                left++;
            }
            ans = Math.max(ans,right-left+1);
            right++;
        }
        return ans;
    }
}
