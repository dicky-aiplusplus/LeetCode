package day07;

/**
 * 给定一个整数数组 arr，返回 arr的最大湍流子数组的长度。
 *
 * 如果比较符号在子数组中的每个相邻元素对之间翻转，则该子数组是湍流子数组。
 *
 * 更正式地来说，当 arr的子数组A[i], A[i+1], ..., A[j]满足仅满足下列条件时，我们称其为湍流子数组：
 *
 * 若i <= k < j：
 * 当 k为奇数时，A[k] > A[k+1]，且
 * 当 k 为偶数时，A[k] < A[k+1]；
 * 或 若i <= k < j：
 * 当 k 为偶数时，A[k] > A[k+1]，且
 * 当 k为奇数时，A[k] < A[k+1]。
 *
 *
 * 示例 1：
 *
 * 输入：arr = [9,4,2,10,7,8,8,1,9]
 * 输出：5
 * 解释：arr[1] > arr[2] < arr[3] > arr[4] < arr[5]
 * 示例 2：
 *
 * 输入：arr = [4,8,12,16]
 * 输出：2
 * 示例 3：
 *
 * 输入：arr = [100]
 * 输出：1
 *
 *
 */
public class Solution6 {
    public int maxTurbulenceSize(int[] arr) {
        int n = arr.length,right = 0, left = 0 , ans = 1;
        while (right<n-1){
            if(right==left){
                if(arr[left]==arr[left+1]){
                    left++;
                }
                right++;
            }else {
                if (arr[right - 1] < arr[right] && arr[right] > arr[right + 1]) {
                    right++;
                } else if (arr[right - 1] > arr[right] && arr[right] < arr[right + 1]) {
                    right++;
                } else {
                    left = right;
                }
            }
            ans= Math.max(ans,right-left);

        }
        return ans;
    }
}
