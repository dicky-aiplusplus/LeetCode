package day07;

/**
 * 给你一个二进制数组nums，你需要从中删掉一个元素。
 *
 * 请你在删掉元素的结果数组中，返回最长的且只包含 1 的非空子数组的长度。
 *
 * 如果不存在这样的子数组，请返回 0 。
 *
 *
 *
 * 提示 1：
 *
 * 输入：nums = [1,1,0,1]
 * 输出：3
 * 解释：删掉位置 2 的数后，[1,1,1] 包含 3 个 1 。
 * 示例 2：
 *
 * 输入：nums = [0,1,1,1,0,1,1,0,1]
 * 输出：5
 * 解释：删掉位置 4 的数字后，[0,1,1,1,1,1,0,1] 的最长全 1 子数组为 [1,1,1,1,1] 。
 * 示例 3：
 *
 * 输入：nums = [1,1,1]
 * 输出：2
 * 解释：你必须要删除一个元素。
 *
 *
 */
public class Solution3 {
    public int longestSubarray(int[] nums) {
        int len = nums.length,right=0,left=0,ans=0,cur=0;
        while (right<len){
            while (right<len&&cur<=1){
                if(nums[right]==0){
                    cur++;
                }
                right++;
                if(cur<=1){
                    ans= Math.max(ans,right-left-1);
                }
            }
            while (left<=right&&cur>1){
                if(nums[left]==0){
                    cur--;
                }
                left++;
            }
        }
        return ans;
    }
}
