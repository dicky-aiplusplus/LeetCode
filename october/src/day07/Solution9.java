package day07;

/**
 * 给你一个字符串S，找出所有长度为K且不含重复字符的子串，请你返回全部满足要求的子串的数目。
 *
 *
 *
 * 示例 1：
 *
 * 输入：S = "havefunonleetcode", K = 5
 * 输出：6
 * 解释：
 * 这里有 6 个满足题意的子串，分别是：'havef','avefu','vefun','efuno','etcod','tcode'。
 * 示例 2：
 *
 * 输入：S = "home", K = 5
 * 输出：0
 * 解释：
 * 注意：K 可能会大于 S 的长度。在这种情况下，就无法找到任何长度为 K 的子串。
 *
 *
 */
public class Solution9 {
    public int numKLenSubstrNoRepeats(String s, int k) {
        int []charT=new int[26];
        int right = 0,left = 0,len = s.length();
        int count = 0;
        while(right<len){
            charT[s.charAt(right)-'a']--;
            if(charT[s.charAt(right)-'a']<-1){
                while(charT[s.charAt(right)-'a']<-1){
                    charT[s.charAt(left)-'a']++;
                    left++;
                }
            }
            if(left+k-1==right){
                charT[s.charAt(left)-'a']++;
                left++;
                count++;
            }
            right++;
        }
        return count;
    }
}
