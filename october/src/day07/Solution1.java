package day07;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 给定一个平衡括号字符串S，按下述规则计算该字符串的分数：
 *
 * () 得 1 分。
 * AB 得A + B分，其中 A 和 B 是平衡括号字符串。
 * (A) 得2 * A分，其中 A 是平衡括号字符串。
 *
 *
 * 示例 1：
 *
 * 输入： "()"
 * 输出： 1
 * 示例 2：
 *
 * 输入： "(())"
 * 输出： 2
 * 示例3：
 *
 * 输入： "()()"
 * 输出： 2
 * 示例4：
 *
 * 输入： "(()(()))"
 * 输出： 6
 *
 *
 */
public class Solution1 {
    public int scoreOfParentheses(String s) {
        Deque<Integer> stack = new LinkedList<>();
        int len = s.length();
        stack.push(0);
        for (int i = 0; i < len; i++) {
            if(s.charAt(i)=='('){
                stack.push(0);
            }else {
                int v = stack.pop();
                int top = stack.pop() + Math.max(2 * v, 1);
                stack.push(top);
            }
        }
        return stack.peek();
    }
}
