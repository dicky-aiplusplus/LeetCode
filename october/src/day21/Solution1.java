package day21;

public class Solution1 {
    public String mergeAlternately(String word1, String word2) {
        char[] chars1 = word1.toCharArray();
        char[] chars2 = word2.toCharArray();
        int maxLen = chars1.length>chars2.length?chars1.length:chars2.length;
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < maxLen; i++) {
            if(i<chars1.length){
                str.append(chars1[i]);
            }
            if(i<chars2.length){
                str.append(chars2[i]);
            }
        }
        return str.toString();
    }
}
