package day21;

/**
 * 给定一个整数 num，将其转化为 7 进制，并以字符串形式输出。
 *
 *
 *
 * 示例 1:
 *
 * 输入: num = 100
 * 输出: "202"
 * 示例 2:
 *
 * 输入: num = -7
 * 输出: "-10"
 *
 *
 *
 */
public class Solution2 {
    StringBuilder str = new StringBuilder();
    public String convertToBase7(int num){
        if(num == 0){
            return "0";
        }
        if(num<0){
            str.append('-');
            num = -num;
        }
        recursion(num);
        return str.toString();
    }
    public void recursion(int num){
        if(num/7==0){
            str.append(num);
            return;
        }
        recursion(num/7);
        str.append(num%7);
    }
}
