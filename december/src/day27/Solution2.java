package day27;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/27 16:07
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 从“A-Z，a-z，0-9，-_”这 64 个字符中，随机生成 6位长的字符串，对于生成的 6位字符串加密，加密方法如下:如果是大写字母就转成小写字母，如果是小写字母就转成大写字母，如果是数字就把数字 N 复制 N+1 遍，如果是-转换成\_，如果是_转换成 \-。举例: A03c-d，加密后应为: a03333C\_D
 */
public class Solution2 {
    public String compile(){
        List<Character> list = new ArrayList<>();
        list.add('-');
        list.add('_');
        for (int i = 0; i < 26; i++) {
            list.add((char) (i+'a'));
            list.add((char) (i+'A'));
        }
        for (int i = 0; i <= 9; i++) {
            list.add((char) (i+'0'));
        }
        Random random = new Random();
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            str.append(list.get(random.nextInt(64)));
        }
        String s = str.toString();
        StringBuilder ans = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(Character.isDigit(c)){
                int n = c-'0';
                for (int j = 0; j <= n; j++) {
                    ans.append(c);
                }
            }else if(c>='A'&&c<='Z'){
                ans.append((char) (c+32));
            }else if(c>='a'&&c<='z'){
                ans.append((char) (c-32));
            }else if(c=='-'){
                ans.append("\\_");
            }else if(c=='_'){
                ans.append("\\-");
            }
        }
        return ans.toString();
    }
}
