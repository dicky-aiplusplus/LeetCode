package day27;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/27 16:52
 */

/**
 * 输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有奇数在数组的前半部分，所有偶数在数组的后半部分。
 *
 *
 *
 * 示例：
 *
 * 输入：nums = [1,2,3,4]
 * 输出：[1,3,2,4]
 * 注：[3,1,2,4] 也是正确的答案之一。
 */
public class Solution3 {
    public int[] exchange(int[] nums) {
        int len = nums.length;
        int []ans = new int[len];
        int left = 0,right = len-1;
        for (int i = 0; i < len; i++) {
            if(nums[i]%2==0){
                ans[right]= nums[i];
                right--;
            }else {
                ans[left] = nums[i];
                left++;
            }
        }
        return ans;
    }
}
