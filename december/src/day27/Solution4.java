package day27;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/27 16:57
 */

import java.util.Arrays;

/**
 * 输入一个递增排序的数组和一个数字s，在数组中查找两个数，使得它们的和正好是s。如果有多对数字的和等于s，则输出任意一对即可。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums = [2,7,11,15], target = 9
 * 输出：[2,7] 或者 [7,2]
 * 示例 2：
 *
 * 输入：nums = [10,26,30,31,47,60], target = 40
 * 输出：[10,30] 或者 [30,10]
 */
public class Solution4 {
    public int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            if(target-nums[i]!=nums[i]){
                int num = binarySearch(nums,target-nums[i]);
                if(num>=0){
                    return new int[]{nums[i],nums[num]};
                }
            }
        }
        return null;
    }
    public int binarySearch(int []nums,int num){
        int left = 0,right = nums.length-1;
        while (left<right){
            int mid  = (right-left)/2 + left;
            if(nums[mid]<num){
                left = mid+1;
            }else if(nums[mid]>num){
                right = mid;
            }else {
                return mid;
            }
        }
        if(nums[left]==num){
            return left;
        }
        return -1;
    }
}
