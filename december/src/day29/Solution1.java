package day29;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/29 10:46
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 给你三个整数数组 nums1、nums2 和 nums3 ，请你构造并返回一个 元素各不相同的 数组，且由 至少 在 两个 数组中出现的所有值组成。数组中的元素可以按 任意 顺序排列。
 *
 *
 * 示例 1：
 *
 * 输入：nums1 = [1,1,3,2], nums2 = [2,3], nums3 = [3]
 * 输出：[3,2]
 * 解释：至少在两个数组中出现的所有值为：
 * - 3 ，在全部三个数组中都出现过。
 * - 2 ，在数组 nums1 和 nums2 中出现过。
 * 示例 2：
 *
 * 输入：nums1 = [3,1], nums2 = [2,3], nums3 = [1,2]
 * 输出：[2,3,1]
 * 解释：至少在两个数组中出现的所有值为：
 * - 2 ，在数组 nums2 和 nums3 中出现过。
 * - 3 ，在数组 nums1 和 nums2 中出现过。
 * - 1 ，在数组 nums1 和 nums3 中出现过。
 * 示例 3：
 *
 * 输入：nums1 = [1,2,2], nums2 = [4,3,3], nums3 = [5]
 * 输出：[]
 * 解释：不存在至少在两个数组中出现的值。
 */
public class Solution1 {
    public List<Integer> twoOutOfThree(int[] nums1, int[] nums2, int[] nums3) {
        Set<Integer> hs = new HashSet<>();
        for (int i = 0; i < nums1.length; i++) {
            hs.add(nums1[i]);
        }
        Set<Integer> ans = new HashSet<>();
        Set<Integer> pre = new HashSet<>();
        for (int i = 0; i < nums2.length; i++) {
            if(hs.contains(nums2[i])){
                ans.add(nums2[i]);
            }else {
                pre.add(nums2[i]);
            }
        }
        for (Integer integer : pre) {
            hs.add(integer);
        }
        for (int i = 0; i < nums3.length; i++) {
            if(hs.contains(nums3[i])){
                ans.add(nums3[i]);
            }
        }
        List<Integer> list = new ArrayList<>();
        for (Integer an : ans) {
            list.add(an);
        }
        return list;
    }
}
