package day29;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/29 15:14
 */

import java.util.Arrays;

/**
 * 从若干副扑克牌中随机抽 5 张牌，判断是不是一个顺子，即这5张牌是不是连续的。2～10为数字本身，A为1，J为11，Q为12，K为13，而大、小王为 0 ，可以看成任意数字。A 不能视为 14。
 *
 *
 *
 * 示例 1:
 *
 * 输入: [1,2,3,4,5]
 * 输出: True
 *
 *
 * 示例 2:
 *
 * 输入: [0,0,1,2,5]
 * 输出: True
 */
public class Solution3 {
    public boolean isStraight(int[] nums) {
        Arrays.sort(nums);
        int k = 0;
        int index = 0;
        while (index<nums.length){
            if(nums[index]==0){
                k++;
            }else if(index>0&&nums[index-1]!=0&&nums[index]==nums[index-1]){
                return false;
            }else if(index>0&&nums[index-1]!=0&&nums[index]-nums[index-1]>1){
                k -= nums[index]-nums[index-1]-1;
                if(k<0){
                    return false;
                }
            }
            index++;
        }
        return true;
    }
}
