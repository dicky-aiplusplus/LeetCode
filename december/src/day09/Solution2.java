package day09;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/9 19:48
 */

/**
 * 给定一个由表示变量之间关系的字符串方程组成的数组，每个字符串方程 equations[i] 的长度为 4，并采用两种不同的形式之一："a==b" 或 "a!=b"。在这里，a 和 b 是小写字母（不一定不同），表示单字母变量名。
 *
 * 只有当可以将整数分配给变量名，以便满足所有给定的方程时才返回 true，否则返回 false。
 *
 *
 *
 * 示例 1：
 *
 * 输入：["a==b","b!=a"]
 * 输出：false
 * 解释：如果我们指定，a = 1 且 b = 1，那么可以满足第一个方程，但无法满足第二个方程。没有办法分配变量同时满足这两个方程。
 * 示例 2：
 *
 * 输入：["b==a","a==b"]
 * 输出：true
 * 解释：我们可以指定 a = 1 且 b = 1 以满足满足这两个方程。
 * 示例 3：
 *
 * 输入：["a==b","b==c","a==c"]
 * 输出：true
 * 示例 4：
 *
 * 输入：["a==b","b!=c","c==a"]
 * 输出：false
 * 示例 5：
 *
 * 输入：["c==c","b==d","x!=z"]
 * 输出：true
 */
public class Solution2 {
    int root[];
    public boolean equationsPossible(String[] equations) {
        root = new int[26];
        for (String equation : equations) {
            int v1 = equation.charAt(0)-'a',v2 = equation.charAt(3)-'a';
            root[v1]=v1;
            root[v2]=v2;
        }
        for (String equation : equations) {
            int v1 = equation.charAt(0)-'a',v2 = equation.charAt(3)-'a';
            if(equation.substring(1,3).equals("==")){
                union(v1,v2);
            }
        }
        for (String equation : equations) {
            int v1 = equation.charAt(0)-'a',v2 = equation.charAt(3)-'a';
            if(equation.substring(1,3).equals("!=")){
                if(find(v1)==find(v2)){
                    return false;
                }
            }
        }
        return true;
    }
    public int find(int x) {
        if (x == root[x]) {
            return x;
        }
        return root[x] = find(root[x]);
    }

    public void union(int x, int y) {
        int rootX = find(x);
        int rootY = find(y);
        if (rootX != rootY) {
            root[rootY] = rootX;
        }
    }
}
