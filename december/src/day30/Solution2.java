package day30;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/30 15:55
 */

/**
 * 实现 pow(x, n) ，即计算 x 的 n 次幂函数（即，xn）。不得使用库函数，同时不需要考虑大数问题。
 *
 *
 *
 * 示例 1：
 *
 * 输入：x = 2.00000, n = 10
 * 输出：1024.00000
 * 示例 2：
 *
 * 输入：x = 2.10000, n = 3
 * 输出：9.26100
 * 示例 3：
 *
 * 输入：x = 2.00000, n = -2
 * 输出：0.25000
 * 解释：2-2 = 1/22 = 1/4 = 0.25
 */
public class Solution2 {
    public double myPow(double x, int n) {
        long nlong =(long) n;
        return n>=0?recursion(x,nlong):1.0/recursion(x,-nlong);
    }
    public double recursion(double x, long n ){
        if(n==0){
            return 1.0;
        }
        double y = recursion(x,n/2);
        return n%2==0?y*y:y*y*x;
    }
}
