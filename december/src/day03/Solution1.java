package day03;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/3 14:54
 */

/**
 * 给你一个混合字符串 s ，请你返回 s 中 第二大 的数字，如果不存在第二大的数字，请你返回 -1 。
 *
 * 混合字符串 由小写英文字母和数字组成。
 *
 *
 *
 * 示例 1：
 *
 * 输入：s = "dfa12321afd"
 * 输出：2
 * 解释：出现在 s 中的数字包括 [1, 2, 3] 。第二大的数字是 2 。
 * 示例 2：
 *
 * 输入：s = "abc1111"
 * 输出：-1
 * 解释：出现在 s 中的数字只包含 [1] 。没有第二大的数字。
 */
public class Solution1 {
    public int secondHighest(String s) {
        int []ans = new int[10];
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(c-'0'>=0&&c-'0'<10){
                ans[c-'0']++;
            }
        }
        int k = 1;
        for (int i = ans.length-1; i >=0 ; i--) {
            if(ans[i]>0&&k==1){
                k--;
            } else if(ans[i]>0&&k==0){
                return i;
            }
        }
        return -1;
    }
}
