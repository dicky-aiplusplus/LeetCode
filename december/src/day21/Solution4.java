package day21;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/21 15:33
 */

import java.util.LinkedList;
import java.util.Queue;

/**
 * 在字符串 s 中找出第一个只出现一次的字符。如果没有，返回一个单空格。 s 只包含小写字母。
 *
 * 示例 1:
 *
 * 输入：s = "abaccdeff"
 * 输出：'b'
 * 示例 2:
 *
 * 输入：s = ""
 * 输出：' '
 */
public class Solution4 {
    public char firstUniqChar(String s) {
        if(s.length()==0){
            return ' ';
        }
        Queue<Character> queue = new LinkedList<>();
        int []chars = new int[26];
        for (int i = 0; i < s.length(); i++) {
            queue.add(s.charAt(i));
            chars[s.charAt(i)-'a']++;
        }
        while (!queue.isEmpty()){
            Character poll = queue.poll();
            if(chars[poll-'a']==1){
                return poll;
            }
        }
        return ' ';
    }
}
