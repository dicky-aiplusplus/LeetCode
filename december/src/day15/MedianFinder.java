package day15;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/15 15:41
 */

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * 中位数是有序整数列表中的中间值。如果列表的大小是偶数，则没有中间值，中位数是两个中间值的平均值。
 *
 * 例如 arr = [2,3,4] 的中位数是 3 。
 * 例如 arr = [2,3] 的中位数是 (2 + 3) / 2 = 2.5 。
 * 实现 MedianFinder 类:
 *
 * MedianFinder() 初始化 MedianFinder 对象。
 *
 * void addNum(int num) 将数据流中的整数 num 添加到数据结构中。
 *
 * double findMedian() 返回到目前为止所有元素的中位数。与实际答案相差 10-5 以内的答案将被接受。
 *
 * 示例 1：
 *
 * 输入
 * ["MedianFinder", "addNum", "addNum", "findMedian", "addNum", "findMedian"]
 * [[], [1], [2], [], [3], []]
 * 输出
 * [null, null, null, 1.5, null, 2.0]
 *
 * 解释
 * MedianFinder medianFinder = new MedianFinder();
 * medianFinder.addNum(1);    // arr = [1]
 * medianFinder.addNum(2);    // arr = [1, 2]
 * medianFinder.findMedian(); // 返回 1.5 ((1 + 2) / 2)
 * medianFinder.addNum(3);    // arr[1, 2, 3]
 * medianFinder.findMedian(); // return 2.0
 */
public class MedianFinder {
    PriorityQueue<Integer> minPq;
    PriorityQueue<Integer> maxPq;
    public MedianFinder() {
        minPq = new PriorityQueue<>((a,b)->(b-a));
        maxPq = new PriorityQueue<>();
    }

    public void addNum(int num) {
        if(minPq.isEmpty()||num<=minPq.peek()){
            minPq.offer(num);
            if(maxPq.size()+1<minPq.size()){
                maxPq.offer(minPq.poll());
            }
        }else {
            maxPq.offer(num);
            if(maxPq.size()>minPq.size()){
                minPq.offer(maxPq.poll());
            }
        }
    }

    public double findMedian() {
        if(minPq.size()> maxPq.size()){
            return minPq.peek();
        }
        return (minPq.peek()+maxPq.peek())/2.0;
    }
}
