package day23;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/23 16:09
 */

/**
 * 输入一个整型数组，数组中的一个或连续多个整数组成一个子数组。求所有子数组的和的最大值。
 *
 * 要求时间复杂度为O(n)。
 *
 *
 *
 * 示例1:
 *
 * 输入: nums = [-2,1,-3,4,-1,2,1,-5,4]
 * 输出: 6
 * 解释: 连续子数组 [4,-1,2,1] 的和最大，为 6。
 *
 */
public class Solution8 {
    public int maxSubArray(int[] nums) {
        int len = nums.length;
        int []dp = new int[len];
        dp[0] = nums[0];
        int maxSum = dp[0];
        for(int i = 1;i<len;i++){
            dp[i]=Math.max(nums[i],dp[i-1]+nums[i]);
            maxSum = Math.max(maxSum,dp[i]);
        }
        return maxSum;
    }
}
