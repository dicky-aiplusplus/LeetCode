package day23;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/23 17:17
 */

import java.util.HashSet;
import java.util.Set;

/**
 * 请从字符串中找出一个最长的不包含重复字符的子字符串，计算该最长子字符串的长度。
 *
 *
 *
 * 示例 1:
 *
 * 输入: "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 * 示例 2:
 *
 * 输入: "bbbbb"
 * 输出: 1
 * 解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
 * 示例 3:
 *
 * 输入: "pwwkew"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
 *      请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
 */
public class Solution11 {
    public int lengthOfLongestSubstring(String s) {
        int maxLen = 0;
        Set<Character> hs = new HashSet<>();
        int slow = 0,fate = 0;
        int preLen = 0;
        while (fate<s.length()){
            if(!hs.contains(s.charAt(fate))){
                hs.add(s.charAt(fate));
                fate++;
                preLen++;
            }else {
                while (hs.contains(s.charAt(fate))){
                    hs.remove(s.charAt(slow));
                    slow++;
                    preLen--;
                }
            }
            maxLen = Math.max(maxLen,preLen);
        }
        return maxLen;
    }
}
