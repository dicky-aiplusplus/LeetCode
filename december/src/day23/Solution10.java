package day23;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/23 16:31
 */

/**
 * 给定一个数字，我们按照如下规则把它翻译为字符串：0 翻译成 “a” ，1 翻译成 “b”，……，11 翻译成 “l”，……，25 翻译成 “z”。一个数字可能有多个翻译。请编程实现一个函数，用来计算一个数字有多少种不同的翻译方法。
 *
 *
 *
 * 示例 1:
 *
 * 输入: 12258
 * 输出: 5
 * 解释: 12258有5种不同的翻译，分别是"bccfi", "bwfi", "bczi", "mcfi"和"mzi"
 */
public class Solution10 {
    int count = 0;
    public int translateNum(int num) {
        recursion(num);
        return count;
    }
    public void recursion(int num){
        if(num < 10){
            count++;
            return;
        }
        if(num%100<26&&num%100>9){
            recursion(num/10);
            recursion(num/100);
        }else{
            recursion(num/10);
        }
    }
}
