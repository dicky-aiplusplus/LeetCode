package day20;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/20 15:44
 */

/**
 * 请实现一个函数，把字符串 s 中的每个空格替换成"%20"。
 *
 *
 *
 * 示例 1：
 *
 * 输入：s = "We are happy."
 * 输出："We%20are%20happy."
 */
public class Solution2 {
    public String replaceSpace(String s) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i)==' '){
                str.append("%20");
            }else {
                str.append(s.charAt(i));
            }
        }
        return str.toString();
    }
}
