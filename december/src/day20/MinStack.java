package day20;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/20 10:10
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 定义栈的数据结构，请在该类型中实现一个能够得到栈的最小元素的 min 函数在该栈中，调用 min、push 及 pop 的时间复杂度都是 O(1)。
 *
 *
 *
 * 示例:
 *
 * MinStack minStack = new MinStack();
 * minStack.push(-2);
 * minStack.push(0);
 * minStack.push(-3);
 * minStack.min();   --> 返回 -3.
 * minStack.pop();
 * minStack.top();      --> 返回 0.
 * minStack.min();   --> 返回 -2.
 */
public class MinStack {
    /** initialize your data structure here. */
    Stack<Integer> stack;
    Stack<List<Integer>> list;
    public MinStack() {
        stack = new Stack<>();
        list = new Stack<>();
    }

    public void push(int x) {
        stack.push(x);
        if(list.isEmpty()){
            List<Integer> ans = new ArrayList<>();
            ans.add(x);
            list.push(ans);
        }else {
            List<Integer> peek = new ArrayList<>(list.peek());
            peek.add(x);
            list.push(peek);
        }
    }

    public void pop() {
        list.pop();
        stack.pop();
    }

    public int top() {
        List<Integer> peek = list.peek();
        return peek.stream().mapToInt(i -> i).max().getAsInt();
    }

    public int min() {
        List<Integer> peek = list.peek();
        return peek.stream().mapToInt(i -> i).min().getAsInt();
    }
}
