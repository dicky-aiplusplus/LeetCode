package day20;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/20 16:53
 */

/**
 * 一个长度为n-1的递增排序数组中的所有数字都是唯一的，并且每个数字都在范围0～n-1之内。在范围0～n-1内的n个数字中有且只有一个数字不在该数组中，请找出这个数字。
 *
 *
 *
 * 示例 1:
 *
 * 输入: [0,1,3]
 * 输出: 2
 * 示例 2:
 *
 * 输入: [0,1,2,3,4,5,6,7,9]
 * 输出: 8
 */
public class Solution6 {
    public int missingNumber(int[] nums) {
        int left = 0,right = nums.length-1;
        while (left<right){
            int mid = (right-left)/2 + left;
            if(mid<nums[mid]){
                right = mid;
            }else {
                left=mid+1;
            }
        }
        if(left==nums.length-1){
            return nums.length-1==nums[nums.length-1]?nums.length:nums.length-1;
        }
        return left;
    }
}
