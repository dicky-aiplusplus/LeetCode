package day14;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/14 16:19
 */

import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * 给你一个会议时间安排的数组 intervals ，每个会议时间都会包括开始和结束的时间 intervals[i] = [starti, endi] ，返回 所需会议室的最小数量 。
 *
 *
 *
 * 示例 1：
 *
 * 输入：intervals = [[0,30],[5,10],[15,20]]
 * 输出：2
 * 示例 2：
 *
 * 输入：intervals = [[7,10],[2,4]]
 * 输出：1
 */
public class Solution2 {
    public int minMeetingRooms(int[][] intervals) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        Arrays.sort(intervals,(o1, o2) -> o1[0]-o2[0]);
        pq.add(intervals[0][1]);
        for (int i = 1; i < intervals.length; i++) {
            if(pq.peek()<=intervals[i][0]){
                pq.poll();
            }
            pq.add(intervals[i][1]);
        }
        return pq.size();
    }
}
