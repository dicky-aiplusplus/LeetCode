package day14;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/14 10:44
 */

import java.util.Arrays;

/**
 * 给你一个 n 个点组成的无向图边集 edgeList ，其中 edgeList[i] = [ui, vi, disi] 表示点 ui 和点 vi 之间有一条长度为 disi 的边。请注意，两个点之间可能有 超过一条边 。
 *
 * 给你一个查询数组queries ，其中 queries[j] = [pj, qj, limitj] ，你的任务是对于每个查询 queries[j] ，判断是否存在从 pj 到 qj 的路径，且这条路径上的每一条边都 严格小于 limitj 。
 *
 * 请你返回一个 布尔数组 answer ，其中 answer.length == queries.length ，当 queries[j] 的查询结果为 true 时， answer 第 j 个值为 true ，否则为 false 。
 *
 *
 *
 * 示例 1：
 *
 *
 * 输入：n = 3, edgeList = [[0,1,2],[1,2,4],[2,0,8],[1,0,16]], queries = [[0,1,2],[0,2,5]]
 * 输出：[false,true]
 * 解释：上图为给定的输入数据。注意到 0 和 1 之间有两条重边，分别为 2 和 16 。
 * 对于第一个查询，0 和 1 之间没有小于 2 的边，所以我们返回 false 。
 * 对于第二个查询，有一条路径（0 -> 1 -> 2）两条边都小于 5 ，所以这个查询我们返回 true 。
 * 示例 2：
 *
 *
 * 输入：n = 5, edgeList = [[0,1,10],[1,2,5],[2,3,9],[3,4,13]], queries = [[0,4,14],[1,4,13]]
 * 输出：[true,false]
 * 解释：上图为给定数据。
 */
public class Solution1 {
    public boolean[] distanceLimitedPathsExist(int n, int[][] edgeList, int[][] queries) {
        Arrays.sort(edgeList,(o1, o2) -> o1[2]-o2[2]);
        Integer []index = new Integer[queries.length];
        for (int i = 0; i < queries.length; i++) {
            index[i] = i;
        }
        Arrays.sort(index,(a,b)-> queries[a][2] - queries[b][2]);
        int root[] = new int[n];
        for (int i = 0; i < n; i++) {
            root[i] = i;
        }
        boolean []res = new boolean[queries.length];
        int k = 0;
        for (int i : index) {
            while (k < edgeList.length && edgeList[k][2] < queries[i][2]) {
                union( edgeList[k][0], edgeList[k][1],root);
                k++;
            }
            res[i] = find(queries[i][0],root) == find(queries[i][1],root);
        }
        return res;
    }
    public int find(int x,int[] root) {
        if (x == root[x]) {
            return x;
        }
        return root[x] = find(root[x],root);
    }

    public void union(int x, int y,int[] root) {
        int rootX = find(x,root);
        int rootY = find(y,root);
        if (rootX != rootY) {
            root[rootY] = rootX;
        }
    }
}
