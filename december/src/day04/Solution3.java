package day04;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/4 10:38
 */

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * 给你一个正整数数组 skill ，数组长度为 偶数 n ，其中 skill[i] 表示第 i 个玩家的技能点。将所有玩家分成 n / 2 个 2 人团队，使每一个团队的技能点之和 相等 。
 * <p>
 * 团队的 化学反应 等于团队中玩家的技能点 乘积 。
 * <p>
 * 返回所有团队的 化学反应 之和，如果无法使每个团队的技能点之和相等，则返回 -1 。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：skill = [3,2,5,1,3,4]
 * 输出：22
 * 解释：
 * 将玩家分成 3 个团队 (1, 5), (2, 4), (3, 3) ，每个团队的技能点之和都是 6 。
 * 所有团队的化学反应之和是 1 * 5 + 2 * 4 + 3 * 3 = 5 + 8 + 9 = 22 。
 * 示例 2：
 * <p>
 * 输入：skill = [3,4]
 * 输出：12
 * 解释：
 * 两个玩家形成一个团队，技能点之和是 7 。
 * 团队的化学反应是 3 * 4 = 12 。
 * 示例 3：
 * <p>
 * 输入：skill = [1,1,2,3]
 * 输出：-1
 * 解释：
 * 无法将玩家分成每个团队技能点都相等的若干个 2 人团队。
 */
public class Solution3 {
    public long dividePlayers(int[] skill) {
        int len = skill.length;
        if (len == 2) {
            return skill[0] * skill[1];
        }
        Arrays.sort(skill);
        int left = 0, right = skill.length - 1;
        int pre = skill[left] + skill[right];
        long sum = 0;
        while (left < right) {
            int i = skill[left] + skill[right];
            int j = skill[left] * skill[right];
            if (i != pre) {
                return -1;
            } else {
                sum += j;
            }
            right--;
            left++;
        }
        return sum;
    }

}
