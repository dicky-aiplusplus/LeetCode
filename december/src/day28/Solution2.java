package day28;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/28 9:20
 */

/**
 * 给定一个 m x n 二维字符网格 board 和一个字符串单词 word 。如果 word 存在于网格中，返回 true ；否则，返回 false 。
 *
 * 单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。
 *
 *
 *
 * 例如，在下面的 3×4 的矩阵中包含单词 "ABCCED"（单词中的字母已标出）。
 *
 *
 *
 *
 *
 * 示例 1：
 *
 * 输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
 * 输出：true
 * 示例 2：
 *
 * 输入：board = [["a","b"],["c","d"]], word = "abcd"
 * 输出：false
 */
public class Solution2 {
    int [][]b = new int[][]{{0,1},{0,-1},{1,0},{-1,0}};
    public boolean exist(char[][] board, String word) {
        boolean [][]f = new boolean[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if(board[i][j]==word.charAt(0)&&backtracking(f,board,i,j,word,0)){
                    return true;
                }
            }
        }
        return false;
    }
    public boolean backtracking(boolean [][]f,char[][] board,int i,int j,String word,int index){
        if(index==word.length()-1&&board[i][j]==word.charAt(index)){
            return true;
        }
        if(index>=word.length()||board[i][j]!=word.charAt(index)){
            return false;
        }
        f[i][j] = true;
        int m = board.length,n = board[0].length;
        for (int k = 0; k < 4; k++) {
            int mi = i+b[k][0];
            int nj = j+b[k][1];
            if(mi>=0&&mi<m&&nj>=0&&nj<n&&f[mi][nj]==false&&backtracking(f,board,mi,nj,word,index+1)){
                return true;
            }
        }
        f[i][j]=false;
        return false;
    }
}
