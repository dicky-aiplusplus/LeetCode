package day28;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/28 9:42
 */

/**
 * 地上有一个m行n列的方格，从坐标 [0,0] 到坐标 [m-1,n-1] 。一个机器人从坐标 [0, 0] 的格子开始移动，它每次可以向左、右、上、下移动一格（不能 移动到方格外），也不能进入行坐标和列坐标的数位之和大于k的格子。例如，当k为18时，机器人能够进入方格 [35, 37] ，因为3+5+3+7=18。但它不能进入方格 [35, 38]，因为3+5+3+8=19。请问该机器人能够到达多少个格子？
 *
 *
 *
 * 示例 1：
 *
 * 输入：m = 2, n = 3, k = 1
 * 输出：3
 * 示例 2：
 *
 * 输入：m = 3, n = 1, k = 0
 * 输出：1
 */
public class Solution3 {
    int count = 0;
    int [][] pre = new int[][]{{0,1},{1,0}};
    public int movingCount(int m, int n, int k) {
        if(k==0){
            return 1;
        }
        boolean [][]f = new boolean[m][n];
        dfs(f,0,0,k);
        return count;
    }
    public void dfs(boolean [][]f,int i,int j,int k){
        if(f[i][j]||!get(i,j,k)){
            return;
        }
        f[i][j]=true;
        count++;
        for (int q = 0; q < 2; q++) {
            if(i+pre[q][0]<f.length&&i+pre[q][0]>=0&&j+pre[q][1]<f[0].length&&j+pre[q][1]>=0){
                dfs(f,i+pre[q][0],j+pre[q][1],k);
            }
        }
    }
    public boolean get(int i,int j, int k){
        int sum = 0;
        while (i!=0){
            sum+=i%10;
            i/=10;
        }
        while (j!=0){
            sum+=j%10;
            j/=10;
        }
        return sum<=k;
    }
}
