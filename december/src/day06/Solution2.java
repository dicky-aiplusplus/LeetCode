package day06;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/6 13:47
 */

/**
 * 路径 被定义为一条从树中任意节点出发，沿父节点-子节点连接，达到任意节点的序列。同一个节点在一条路径序列中 至多出现一次 。该路径 至少包含一个 节点，且不一定经过根节点。
 *
 * 路径和 是路径中各节点值的总和。
 *
 * 给你一个二叉树的根节点 root ，返回其 最大路径和 。
 *
 *
 *
 * 示例 1：
 *
 *
 * 输入：root = [1,2,3]
 * 输出：6
 * 解释：最优路径是 2 -> 1 -> 3 ，路径和为 2 + 1 + 3 = 6
 * 示例 2：
 *
 *
 * 输入：root = [-10,9,20,null,null,15,7]
 * 输出：42
 * 解释：最优路径是 15 -> 20 -> 7 ，路径和为 15 + 20 + 7 = 42
 */
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

public class Solution2 {
    int maxSum = Integer.MIN_VALUE;
    public int maxPathSum(TreeNode root) {
        return Math.max(dfs(root),maxSum);
    }
    public int dfs(TreeNode root){
        if(root==null){
            return 0;
        }
        int leftSum = root.val+dfs(root.left);
        int rightSum = root.val+dfs(root.right);
        maxSum = Math.max(leftSum+rightSum-root.val,Math.max(maxSum,Math.max(leftSum,Math.max(rightSum,root.val))));
        return Math.max(leftSum,Math.max(rightSum,root.val));
    }
}
