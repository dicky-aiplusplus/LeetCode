package day18;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/18 19:52
 */

import java.util.HashMap;
import java.util.Map;

/**
 * Trie（发音类似 "try"）或者说 前缀树 是一种树形数据结构，用于高效地存储和检索字符串数据集中的键。这一数据结构有相当多的应用情景，例如自动补完和拼写检查。
 *
 * 请你实现 Trie 类：
 *
 * Trie() 初始化前缀树对象。
 * void insert(String word) 向前缀树中插入字符串 word 。
 * boolean search(String word) 如果字符串 word 在前缀树中，返回 true（即，在检索之前已经插入）；否则，返回 false 。
 * boolean startsWith(String prefix) 如果之前已经插入的字符串 word 的前缀之一为 prefix ，返回 true ；否则，返回 false 。
 *
 *
 * 示例：
 *
 * 输入
 * ["Trie", "insert", "search", "search", "startsWith", "insert", "search"]
 * [[], ["apple"], ["apple"], ["app"], ["app"], ["app"], ["app"]]
 * 输出
 * [null, null, true, false, true, null, true]
 *
 * 解释
 * Trie trie = new Trie();
 * trie.insert("apple");
 * trie.search("apple");   // 返回 True
 * trie.search("app");     // 返回 False
 * trie.startsWith("app"); // 返回 True
 * trie.insert("app");
 * trie.search("app");     // 返回 True
 */
public class Tries {
    Map<String,Object> map;
    public Tries() {
        map = new HashMap<>();
    }

    public void insert(String word) {
        int len = word.length();
        Map<String, Object> curMap = map;
        for (int i = 0; i < len; i++) {
            String key = String.valueOf(word.charAt(i));
            // 当前字在当前层是否存在, 不存在则新建, 当前层数据指向下一个节点, 继续判断是否存在数据
            Map<String, Object> wordMap = (Map<String, Object>) curMap.get(key);
            if (wordMap == null) {
                // 每个节点存在两个数据: 下一个节点和isEnd(是否结束标志)
                wordMap = new HashMap<>(2);
                wordMap.put("isEnd", "0");
                curMap.put(key, wordMap);
            }
            curMap = wordMap;
            // 如果当前字是词的最后一个字，则将isEnd标志置1
            if (i == len -1) {
                curMap.put("isEnd", "1");
            }
        }
    }

    public boolean search(String word) {
        int len = word.length();
        Map<String, Object> curMap = map;
        for (int i = 0; i < len; i++) {
            String key = String.valueOf(word.charAt(i));
            // 当前字在当前层是否存在, 不存在则新建, 当前层数据指向下一个节点, 继续判断是否存在数据
            Map<String, Object> wordMap = (Map<String, Object>) curMap.get(key);
            if(wordMap==null){
                return false;
            }
            curMap = wordMap;
            if(i==len-1&&curMap.get("isEnd").equals("1")){
                return true;
            }
        }
        return false;
    }

    public boolean startsWith(String prefix) {
        int len = prefix.length();
        Map<String, Object> curMap = map;
        for (int i = 0; i < len; i++) {
            String key = String.valueOf(prefix.charAt(i));
            // 当前字在当前层是否存在, 不存在则新建, 当前层数据指向下一个节点, 继续判断是否存在数据
            Map<String, Object> wordMap = (Map<String, Object>) curMap.get(key);
            if(wordMap==null){
                return false;
            }
            curMap = wordMap;
        }
        return true;
    }
}
