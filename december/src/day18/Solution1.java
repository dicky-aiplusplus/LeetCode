package day18;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/18 10:18
 */

import java.util.HashSet;
import java.util.Set;

/**
 * 给你一个下标从 0 开始的字符串数组 words 。
 *
 * 如果两个字符串由相同的字符组成，则认为这两个字符串 相似 。
 *
 * 例如，"abca" 和 "cba" 相似，因为它们都由字符 'a'、'b'、'c' 组成。
 * 然而，"abacba" 和 "bcfd" 不相似，因为它们不是相同字符组成的。
 * 请你找出满足字符串 words[i] 和 words[j] 相似的下标对 (i, j) ，并返回下标对的数目，其中 0 <= i < j <= word.length - 1 。
 *
 *
 *
 * 示例 1：
 *
 * 输入：words = ["aba","aabb","abcd","bac","aabc"]
 * 输出：2
 * 解释：共有 2 对满足条件：
 * - i = 0 且 j = 1 ：words[0] 和 words[1] 只由字符 'a' 和 'b' 组成。
 * - i = 3 且 j = 4 ：words[3] 和 words[4] 只由字符 'a'、'b' 和 'c' 。
 * 示例 2：
 *
 * 输入：words = ["aabb","ab","ba"]
 * 输出：3
 * 解释：共有 3 对满足条件：
 * - i = 0 且 j = 1 ：words[0] 和 words[1] 只由字符 'a' 和 'b' 组成。
 * - i = 0 且 j = 2 ：words[0] 和 words[2] 只由字符 'a' 和 'b' 组成。
 * - i = 1 且 j = 2 ：words[1] 和 words[2] 只由字符 'a' 和 'b' 组成。
 * 示例 3：
 *
 * 输入：words = ["nba","cba","dba"]
 * 输出：0
 * 解释：不存在满足条件的下标对，返回 0 。
 */
public class Solution1 {
    public int similarPairs(String[] words) {
        int ans = 0;
        for (int i = 0; i < words.length; i++) {
            Set<Character> hs = new HashSet<>();
            for (int i1 = 0; i1 < words[i].length(); i1++) {
                hs.add(words[i].charAt(i1));
            }
            int size = hs.size();
            loop:for (int j = i+1; j < words.length; j++) {
                Set<Character> hs1 = new HashSet<>();
                for (int i1 = 0; i1 < words[j].length(); i1++) {
                    if(hs.contains(words[j].charAt(i1))){
                        hs1.add(words[j].charAt(i1));
                    }else{
                        continue loop;
                    }
                }
                if(hs1.size()==size){
                    ans++;
                }
            }
        }
        return ans;
    }
}
