package day01;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/1 9:40
 */

/**
 * 给出两个一维的向量，请你实现一个迭代器，交替返回它们中间的元素。
 *
 * 示例:
 *
 * 输入:
 * v1 = [1,2]
 * v2 = [3,4,5,6]
 *
 * 输出: [1,3,2,4,5,6]
 *
 * 解析: 通过连续调用 next 函数直到 hasNext 函数返回 false，
 *      next 函数返回值的次序应依次为: [1,3,2,4,5,6]。
 * 拓展：假如给你 k 个一维向量呢？你的代码在这种情况下的扩展性又会如何呢?
 *
 * 拓展声明：
 *  “锯齿” 顺序对于 k > 2 的情况定义可能会有些歧义。所以，假如你觉得 “锯齿” 这个表述不妥，也可以认为这是一种 “循环”。例如：
 *
 * 输入:
 * [1,2,3]
 * [4,5,6,7]
 * [8,9]
 *
 * 输出: [1,4,8,2,5,9,3,6,7].
 */
public class ZigzagIterator {
    ArrayList<Integer>[] listArr;
    int i;
    int j;
    int size;

    public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
        listArr = new ArrayList[2];
        listArr[0] = new ArrayList<>(v1);
        listArr[1] = new ArrayList<>(v2);
        size = Math.max(v1.size(), v2.size());
        i = v1.size() > 0 ? 0 : 1;
        j = 0;
    }

    public int next() {
        int ans = listArr[i].get(j);
        moveToNext();
        return ans;
    }

    public boolean hasNext() {
        return i < listArr.length && j < size;
    }

    private void moveToNext() {
        movePoint();
        if (j >= listArr[i].size()) {
            movePoint();
        }
    }

    private void movePoint() {
        i += 1;
        if (i == listArr.length) {
            i = 0;
            j += 1;
        }
    }
}
