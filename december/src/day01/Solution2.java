package day01;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/1 11:34
 */

import java.util.LinkedList;
import java.util.List;

/**
 * 给定一个整数数组 temperatures ，表示每天的温度，返回一个数组 answer ，其中 answer[i] 是指对于第 i 天，下一个更高温度出现在几天后。如果气温在这之后都不会升高，请在该位置用 0 来代替。
 *
 *
 *
 * 示例 1:
 *
 * 输入: temperatures = [73,74,75,71,69,72,76,73]
 * 输出: [1,1,4,2,1,1,0,0]
 * 示例 2:
 *
 * 输入: temperatures = [30,40,50,60]
 * 输出: [1,1,1,0]
 * 示例 3:
 *
 * 输入: temperatures = [30,60,90]
 * 输出: [1,1,0]
 */
public class Solution2 {
    public int[] dailyTemperatures(int[] temperatures) {
        LinkedList<Integer> stack = new LinkedList<>();
        int []ans = new int[temperatures.length];
        for (int i = 0; i < temperatures.length; i++) {
            if(stack.isEmpty()){
                stack.add(i);
            }
            while (!stack.isEmpty()&&temperatures[i]>temperatures[stack.peekLast()]){
                Integer integer = stack.pollLast();
                ans[integer] = i-integer;
            }
            stack.add(i);
        }
        while (!stack.isEmpty()){
            ans[stack.pollLast()]=0;
        }
        return ans;
    }
}
