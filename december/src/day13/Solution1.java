package day13;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/12/13 8:26
 */

import java.util.HashSet;
import java.util.Set;

/**
 *全字母句 指包含英语字母表中每个字母至少一次的句子。
 *
 * 给你一个仅由小写英文字母组成的字符串 sentence ，请你判断 sentence 是否为 全字母句 。
 *
 * 如果是，返回 true ；否则，返回 false 。
 *
 *
 *
 * 示例 1：
 *
 * 输入：sentence = "thequickbrownfoxjumpsoverthelazydog"
 * 输出：true
 * 解释：sentence 包含英语字母表中每个字母至少一次。
 * 示例 2：
 *
 * 输入：sentence = "leetcode"
 * 输出：false
 */
public class Solution1 {
    public boolean checkIfPangram(String sentence) {
        Set<Character> hs = new HashSet<>();
        for (int i = 0; i < sentence.length(); i++) {
            hs.add(sentence.charAt(i));
        }
        return hs.size()==26;
    }
}
