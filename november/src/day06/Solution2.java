package day06;

import sun.misc.LRUCache;

import java.util.HashSet;
import java.util.Set;

/**
 *
 通过的用户数7
 尝试过的用户数8
 用户总通过次数7
 用户总提交次数9
 题目难度Medium
 给你一个整数数组 nums 和一个整数 k 。请你从 nums 中满足下述条件的全部子数组中找出最大子数组和：

 子数组的长度是 k，且
 子数组中的所有元素 各不相同 。
 返回满足题面要求的最大子数组和。如果不存在子数组满足这些条件，返回 0 。

 子数组 是数组中一段连续非空的元素序列。



 示例 1：

 输入：nums = [1,5,4,2,9,9,9], k = 3
 输出：15
 解释：nums 中长度为 3 的子数组是：
 - [1,5,4] 满足全部条件，和为 10 。
 - [5,4,2] 满足全部条件，和为 11 。
 - [4,2,9] 满足全部条件，和为 15 。
 - [2,9,9] 不满足全部条件，因为元素 9 出现重复。
 - [9,9,9] 不满足全部条件，因为元素 9 出现重复。
 因为 15 是满足全部条件的所有子数组中的最大子数组和，所以返回 15 。
 示例 2：

 输入：nums = [4,4,4], k = 3
 输出：0
 解释：nums 中长度为 3 的子数组是：
 - [4,4,4] 不满足全部条件，因为元素 4 出现重复。
 因为不存在满足全部条件的子数组，所以返回 0 。
 */
public class Solution2 {
    public static long maximumSubarraySum(int[] nums, int k) {
        int left = 0,right = 0;
        int len = nums.length;
        long sumMax = 0;
        long sum = 0;
        Set<Integer> hs = new HashSet<>();
        while (right<len){
            if(hs.contains(nums[right])){
                while (nums[left]!=nums[right]){
                    sum-=nums[left];
                    hs.remove(nums[left]);
                    left++;
                }
                left++;
                right++;
                continue;
            }
            if(right-left<=k-1){
                sum+=nums[right];
                hs.add(nums[right]);
                if(right- left==k-1){
                    sumMax = Math.max(sum,sumMax);
                }
                right++;
            } else if(right-left>k-1){
                sum=sum-nums[left]+nums[right];
                hs.remove(nums[left]);
                hs.add(nums[right]);
                sumMax = Math.max(sum,sumMax);
                left++;
                right++;
            }
        }
        return sumMax;
    }

    public static void main(String[] args) {
        System.out.println(maximumSubarraySum(new int[]{9,18,10,13,17,9,19,2,1,18
        },5));
    }
}
