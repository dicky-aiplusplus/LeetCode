package day01;

import java.util.HashSet;
import java.util.Set;

/**
 * 石头的类型，你想知道你拥有的石头中有多少是宝石。
 *
 * 字母区分大小写，因此 "a" 和 "A" 是不同类型的石头。
 *
 *
 *
 * 示例 1：
 *
 * 输入：jewels = "aA", stones = "aAAbbbb"
 * 输出：3
 * 示例 2：
 *
 * 输入：jewels = "z", stones = "ZZ"
 * 输出：0
 *
 *
 */
public class Solution5 {
    public int numJewelsInStones(String jewels, String stones) {
        Set<Character> hs = new HashSet<>();
        char[] chars = jewels.toCharArray();
        for (char aChar : chars) {
            hs.add(aChar);
        }
        int ans = 0;
        char[] chars1 = stones.toCharArray();
        for (char c : chars1) {
            if(hs.contains(c)){
                ans++;
            }
        }
        return ans;
    }
}
