package day01;

import java.util.HashMap;
import java.util.Map;

/**
 * 给你一个整数数组nums 和一个整数k ，判断数组中是否存在两个 不同的索引i和j ，满足 nums[i] == nums[j] 且 abs(i - j) <= k 。如果存在，返回 true ；否则，返回 false 。
 *
 *
 *
 * 示例1：
 *
 * 输入：nums = [1,2,3,1], k = 3
 * 输出：true
 * 示例 2：
 *
 * 输入：nums = [1,0,1,1], k = 1
 * 输出：true
 * 示例 3：
 *
 * 输入：nums = [1,2,3,1,2,3], k = 2
 * 输出：false
 *
 *
 */
public class Solution1 {
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer,Integer> leftMap = new HashMap<>(),rightMap = new HashMap<>();
        int right = nums.length-1;
        int left = 0;
        while (left<right){
            if(leftMap.containsKey(nums[left])&&Math.abs(left-leftMap.get(nums[left]))<=k){
                return true;
            }
            leftMap.put(nums[left],left);
            if(rightMap.containsKey(nums[left])&&Math.abs(left-rightMap.get(nums[left]))<=k){
                return true;
            }
            if(rightMap.containsKey(nums[right])&&Math.abs(right-rightMap.get(nums[right]))<=k){
                return true;
            }
            rightMap.put(nums[right],right);
            if(leftMap.containsKey(nums[right])&&Math.abs(right-leftMap.get(nums[right]))<=k){
                return true;
            }
            right--;
            left++;
        }
        if(right==left){
            if((leftMap.containsKey(nums[left])&&Math.abs(left-leftMap.get(nums[left]))<=k)||rightMap.containsKey(nums[left])&&Math.abs(left-rightMap.get(nums[left]))<=k){
                return true;
            }
        }
        return false;
    }
}
