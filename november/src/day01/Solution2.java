package day01;

import java.nio.charset.StandardCharsets;

/**
 * 给你两个字符串数组 word1 和 word2 。如果两个数组表示的字符串相同，返回 true ；否则，返回 false 。
 *
 * 数组表示的字符串是由数组中的所有元素 按顺序 连接形成的字符串。
 *
 *
 *
 * 示例 1：
 *
 * 输入：word1 = ["ab", "c"], word2 = ["a", "bc"]
 * 输出：true
 * 解释：
 * word1 表示的字符串为 "ab" + "c" -> "abc"
 * word2 表示的字符串为 "a" + "bc" -> "abc"
 * 两个字符串相同，返回 true
 * 示例 2：
 *
 * 输入：word1 = ["a", "cb"], word2 = ["ab", "c"]
 * 输出：false
 * 示例 3：
 *
 * 输入：word1  = ["abc", "d", "defg"], word2 = ["abcddefg"]
 * 输出：true
 *
 *
 */
public class Solution2 {
    public boolean arrayStringsAreEqual(String[] word1, String[] word2) {
        StringBuilder strWord1 = new StringBuilder(),strWord2 = new StringBuilder();
        for (String s : word1) {
            strWord1.append(s);
        }
        for (String s : word2) {
            strWord2.append(s);
        }
        return strWord1.toString().equals(strWord2.toString());
    }
}
