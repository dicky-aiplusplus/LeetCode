package day02;

/**
 * 给定一个不为空的二叉搜索树和一个目标值 target，请在该二叉搜索树中找到最接近目标值 target 的数值。
 * <p>
 * 注意：
 * <p>
 * 给定的目标值 target 是一个浮点数
 * 题目保证在该二叉搜索树中只会存在一个最接近目标值的数
 * 示例：
 * <p>
 * 输入: root = [4,2,5,1,3]，目标值 target = 3.714286
 * <p>
 * 4
 * / \
 * 2   5
 * / \
 * 1   3
 * <p>
 * 输出: 4
 */
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

public class Solution3 {
    public int closestValue(TreeNode root, double target) {
        if (root.val-target==0){
            return root.val;
        }
        int res= root.val;
        while (root!=null){
            if (Math.abs(root.val-target)<Math.abs(res-target)){
                res=root.val;
            }
            if (root.val>target){
                root=root.left;
            }
            else {
                root=root.right;
            }
        }
        return res;
    }
}
