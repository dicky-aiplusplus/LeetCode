package day02;

/**
 * 给定一个 正整数 num ，编写一个函数，如果 num 是一个完全平方数，则返回 true ，否则返回 false 。
 *
 * 进阶：不要 使用任何内置的库函数，如 sqrt 。
 *
 *
 *
 * 示例 1：
 *
 * 输入：num = 16
 * 输出：true
 * 示例 2：
 *
 * 输入：num = 14
 * 输出：false
 *
 *
 */
public class Solution4 {
    public boolean isPerfectSquare(int num) {
        int left = 1,right = num;
        while (left<=right){
            int mid = (right-left)/2 +left;
            if(num%mid==0&&mid==num/mid){
                return true;
            }else if(mid<num/mid){
                left = mid+1;
            }else {
                right = mid-1;
            }
        }
        return false;
    }
}
