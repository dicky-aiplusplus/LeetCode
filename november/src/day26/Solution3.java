package day26;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/11/26 22:37
 */

/**
 * 给你一个下标从 0 开始的 m x n 二进制矩阵 grid 。
 *
 * 我们按照如下过程，定义一个下标从 0 开始的 m x n 差值矩阵 diff ：
 *
 * 令第 i 行一的数目为 onesRowi 。
 * 令第 j 列一的数目为 onesColj 。
 * 令第 i 行零的数目为 zerosRowi 。
 * 令第 j 列零的数目为 zerosColj 。
 * diff[i][j] = onesRowi + onesColj - zerosRowi - zerosColj
 * 请你返回差值矩阵 diff 。
 *
 *
 *
 * 示例 1：
 *
 *
 *
 * 输入：grid = [[0,1,1],[1,0,1],[0,0,1]]
 * 输出：[[0,0,4],[0,0,4],[-2,-2,2]]
 * 解释：
 * - diff[0][0] = onesRow0 + onesCol0 - zerosRow0 - zerosCol0 = 2 + 1 - 1 - 2 = 0
 * - diff[0][1] = onesRow0 + onesCol1 - zerosRow0 - zerosCol1 = 2 + 1 - 1 - 2 = 0
 * - diff[0][2] = onesRow0 + onesCol2 - zerosRow0 - zerosCol2 = 2 + 3 - 1 - 0 = 4
 * - diff[1][0] = onesRow1 + onesCol0 - zerosRow1 - zerosCol0 = 2 + 1 - 1 - 2 = 0
 * - diff[1][1] = onesRow1 + onesCol1 - zerosRow1 - zerosCol1 = 2 + 1 - 1 - 2 = 0
 * - diff[1][2] = onesRow1 + onesCol2 - zerosRow1 - zerosCol2 = 2 + 3 - 1 - 0 = 4
 * - diff[2][0] = onesRow2 + onesCol0 - zerosRow2 - zerosCol0 = 1 + 1 - 2 - 2 = -2
 * - diff[2][1] = onesRow2 + onesCol1 - zerosRow2 - zerosCol1 = 1 + 1 - 2 - 2 = -2
 * - diff[2][2] = onesRow2 + onesCol2 - zerosRow2 - zerosCol2 = 1 + 3 - 2 - 0 = 2
 * 示例 2：
 *
 *
 *
 * 输入：grid = [[1,1,1],[1,1,1]]
 * 输出：[[5,5,5],[5,5,5]]
 * 解释：
 * - diff[0][0] = onesRow0 + onesCol0 - zerosRow0 - zerosCol0 = 3 + 2 - 0 - 0 = 5
 * - diff[0][1] = onesRow0 + onesCol1 - zerosRow0 - zerosCol1 = 3 + 2 - 0 - 0 = 5
 * - diff[0][2] = onesRow0 + onesCol2 - zerosRow0 - zerosCol2 = 3 + 2 - 0 - 0 = 5
 * - diff[1][0] = onesRow1 + onesCol0 - zerosRow1 - zerosCol0 = 3 + 2 - 0 - 0 = 5
 * - diff[1][1] = onesRow1 + onesCol1 - zerosRow1 - zerosCol1 = 3 + 2 - 0 - 0 = 5
 * - diff[1][2] = onesRow1 + onesCol2 - zerosRow1 - zerosCol2 = 3 + 2 - 0 - 0 = 5
 */
public class Solution3 {
    public int[][] onesMinusZeros(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        int [][]diff = new int[m][n];
        int [][]row = new int[m][2];
        int [][]col = new int[n][2];
        for (int i = 0; i < m; i++) {
            int sum = 0;
            for (int j = 0; j < n; j++) {
                sum+=grid[i][j];
            }
            row[i][0]=m-sum;
            row[i][1]=sum;
        }
        for (int i = 0; i < n; i++) {
            int sum = 0;
            for (int j = 0; j < m; j++) {
                sum+=grid[j][i];
            }
            col[i][0]=n-sum;
            col[i][1]=sum;
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                diff[i][j]=row[i][1]+col[j][1]-row[i][0]-col[j][0];
            }
        }
        return diff;
    }
}
