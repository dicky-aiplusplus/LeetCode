package day08;

/**
 * 输入一个非负整数数组，把数组里所有数字拼接起来排成一个数，打印能拼接出的所有数字中最小的一个。
 *
 *
 *
 * 示例 1:
 *
 * 输入: [10,2]
 * 输出: "102"
 * 示例 2:
 *
 * 输入: [3,30,34,5,9]
 * 输出: "3033459"
 */
public class Solution5 {
    public String minNumber(int[] nums) {
        int n = nums.length;
        StringBuilder str = new StringBuilder();
        for (int i = 1; i < n; i++) {
            for(int j = i;j>0;j--){
                String str1 = String.valueOf(nums[j])+nums[j-1];
                String str2 = String.valueOf(nums[j-1])+nums[j];
                if(Long.parseLong(str1)< Long.parseLong(str2)){
                    int temp = nums[j];
                    nums[j]=nums[j-1];
                    nums[j-1]=temp;
                }else {
                    break;
                }
            }
        }
        for (int num : nums) {
            str.append(num);
        }
        return str.toString();
    }
}
