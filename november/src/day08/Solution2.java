package day08;
/*
给你一个整数数组 nums，返回 数组 answer ，其中 answer[i] 等于 nums 中除 nums[i] 之外其余各元素的乘积 。

题目数据 保证 数组 nums之中任意元素的全部前缀元素和后缀的乘积都在  32 位 整数范围内。

请不要使用除法，且在 O(n) 时间复杂度内完成此题。



示例 1:

输入: nums = [1,2,3,4]
输出: [24,12,8,6]
示例 2:

输入: nums = [-1,1,0,-3,3]
输出: [0,0,9,0,0]


提示：

2 <= nums.length <= 105
-30 <= nums[i] <= 30
保证 数组 nums之中任意元素的全部前缀元素和后缀的乘积都在  32 位 整数范围内
 */
public class Solution2 {
    public int[] productExceptSelf(int[] nums) {
        int n = nums.length;
        int []prefix=new int[n];
        prefix[0]=1;
        for(int i=0;i<n-1;i++){
            prefix[i+1]=prefix[i]*nums[i];
        }
        int []suffix=new int[n];
        suffix[n-1]=1;
        for(int i = n-1;i>0;i--){
            suffix[i-1]=suffix[i]*nums[i];
        }
        for(int i = 0;i < n;i++){
            nums[i] = prefix[i]*suffix[i];
        }
        return nums;
    }
}
