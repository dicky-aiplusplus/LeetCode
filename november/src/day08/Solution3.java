package day08;

/**
 * 给你一个整数数组 nums 和一个整数 k ，请你统计并返回 该数组中和为 k 的连续子数组的个数 。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums = [1,1,1], k = 2
 * 输出：2
 * 示例 2：
 *
 * 输入：nums = [1,2,3], k = 3
 * 输出：2
 *
 *
 * 提示：
 *
 * 1 <= nums.length <= 2 * 104
 * -1000 <= nums[i] <= 1000
 * -107 <= k <= 107
 */
public class Solution3 {
    public int subarraySum(int[] nums, int k) {
        int n = nums.length;
        if(n==1){
            return k==nums[0]?1:0;
        }
        int []ans = new int[n+1];
        ans[0]=0;
        for(int i=0;i<n;i++){
            ans[i+1]=ans[i]+nums[i];
        }
        int sum = 0;
        for(int i=1;i<n+1;i++){
            for(int j = i-1;j>=0;j--){
                if(ans[i]-ans[j]==k){
                    sum++;
                }
            }
        }
        return sum;
    }
}
