package day24;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 给你一个含 n 个整数的数组 nums ，其中 nums[i] 在区间 [1, n] 内。请你找出所有在 [1, n] 范围内但没有出现在 nums 中的数字，并以数组的形式返回结果。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums = [4,3,2,7,8,2,3,1]
 * 输出：[5,6]
 * 示例 2：
 *
 * 输入：nums = [1,1]
 * 输出：[2]
 */
public class Solution3 {
    public List<Integer> findDisappearedNumbers(int[] nums) {
        int n = nums.length;
        Set<Integer> hs = new HashSet<>();
        for (int i = 0;i<n;i++){
            hs.add(nums[i]);
        }
        List<Integer> list = new ArrayList<>();
        for(int i = 1;i<=n;i++){
            if(!hs.contains(i)){
                list.add(i);
            }
        }
        return list;
    }
}
