package day28;

/**
 * @author aiPlusPlus
 * @version 1.0
 * @date 2022/11/28 20:38
 */

import com.sun.org.apache.bcel.internal.generic.NEW;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个用链表表示的非负整数， 然后将这个整数 再加上 1 。
 *
 * 这些数字的存储是这样的：最高位有效的数字位于链表的首位 head 。
 *
 *
 *
 * 示例 1:
 *
 * 输入: head = [1,2,3]
 * 输出: [1,2,4]
 * 示例 2:
 *
 * 输入: head = [0]
 * 输出: [1]
 */
class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }
public class Solution4 {
    public ListNode plusOne(ListNode head) {
        /*
        数据多了会超时不建议
        List<Integer> list = new ArrayList<>();
        list.add(0);
        while (head!=null){
            list.add(head.val);
            head=head.next;
        }
        int size = list.size();
        for(int i = size-1;i>=0;i--){
            list.set(i,list.get(i)+1);
            if(list.get(i)>=10){
                list.set(i,list.get(i)-10);
            }else {
                break;
            }
        }
        boolean f = false;
        ListNode listNode = new ListNode();
        return list.get(0)==0?pre(1,list):pre(0,list);
    }
    public ListNode pre(int i,List<Integer> list){
        if(list.size()-i==0){
            return null;
        }
        ListNode listNode = new ListNode(list.get(i));
        listNode.next = pre(i+1,list);
        return listNode;*/

        ListNode listNode = new ListNode(0);
        listNode.next=head;
        ListNode node = listNode;
        while (head!=null){
            if(head.val!=9){
                node = head;
            }
            head = head.next;
        }
        node.val++;
        node = node.next;
        while (node!=null){
            node.val=0;
            node = node.next;
        }
        return listNode.val==0?listNode.next:listNode;
    }
}
