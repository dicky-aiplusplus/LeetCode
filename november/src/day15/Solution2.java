package day15;

/**
 * 给你一个整数数组 nums ，请你找出数组中乘积最大的非空连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。
 *
 * 测试用例的答案是一个 32-位 整数。
 *
 * 子数组 是数组的连续子序列。
 *
 *
 *
 * 示例 1:
 *
 * 输入: nums = [2,3,-2,4]
 * 输出: 6
 * 解释: 子数组 [2,3] 有最大乘积 6。
 * 示例 2:
 *
 * 输入: nums = [-2,0,-1]
 * 输出: 0
 * 解释: 结果不能为 2, 因为 [-2,-1] 不是子数组。
 */
public class Solution2 {
    public int maxProduct(int[] nums) {
        int n = nums.length;
        int maxF[] = new int[n];
        maxF[0] = nums[0];
        int minF[] = new int[n];
        minF[0] = nums[0];
        int maxMultiplication = nums[0];
        for (int i = 1; i < n; i++) {
            maxF[i] = Math.max(nums[i],nums[i]*maxF[i-1]);
            maxF[i] = Math.max(nums[i]*minF[i-1],maxF[i]);
            maxMultiplication = Math.max(maxMultiplication,maxF[i]);
            minF[i] = Math.min(nums[i],nums[i]*minF[i-1]);
            minF[i] = Math.min(nums[i]*maxF[i-1],minF[i]);
        }
        return maxMultiplication;
    }
}
