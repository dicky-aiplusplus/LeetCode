package day23;

/**
 * 给你一个正整数数组 arr ，请你计算所有可能的奇数长度子数组的和。
 *
 * 子数组 定义为原数组中的一个连续子序列。
 *
 * 请你返回 arr 中 所有奇数长度子数组的和 。
 *
 *
 *
 * 示例 1：
 *
 * 输入：arr = [1,4,2,5,3]
 * 输出：58
 * 解释：所有奇数长度子数组和它们的和为：
 * [1] = 1
 * [4] = 4
 * [2] = 2
 * [5] = 5
 * [3] = 3
 * [1,4,2] = 7
 * [4,2,5] = 11
 * [2,5,3] = 10
 * [1,4,2,5,3] = 15
 * 我们将所有值求和得到 1 + 4 + 2 + 5 + 3 + 7 + 11 + 10 + 15 = 58
 * 示例 2：
 *
 * 输入：arr = [1,2]
 * 输出：3
 * 解释：总共只有 2 个长度为奇数的子数组，[1] 和 [2]。它们的和为 3 。
 * 示例 3：
 *
 * 输入：arr = [10,11,12]
 * 输出：66
 */
public class Solution4 {
    public int sumOddLengthSubarrays(int[] arr) {
        int len = arr.length;
        int []preSum = new int[len+1];
        for (int i = 0; i < len; i++) {
            preSum[i+1]=preSum[i]+arr[i];
        }
        int sum = 0;
        for (int i = 0; i < len; i++) {
            for (int j = 1; i+j <= len; j+=2) {
                int index = i+j-1;
                sum+=preSum[index+1]-preSum[i];
            }
        }
        return sum;
    }
}
