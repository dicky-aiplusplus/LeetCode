package day23;

import java.util.HashSet;
import java.util.Set;

/**
 * 给定一个未排序的整数数组 nums ，找出数字连续的最长序列（不要求序列元素在原数组中连续）的长度。
 *
 * 请你设计并实现时间复杂度为 O(n) 的算法解决此问题。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums = [100,4,200,1,3,2]
 * 输出：4
 * 解释：最长数字连续序列是 [1, 2, 3, 4]。它的长度为 4。
 * 示例 2：
 *
 * 输入：nums = [0,3,7,2,5,8,4,6,0,1]
 * 输出：9
 */

public class Solution6 {
    public int longestConsecutive(int[] nums) {
        //用并查集就是傻子，超内存了
        Set<Integer> hs = new HashSet<>();
        for (int num : nums) {
            hs.add(num);
        }
        int maxLen = 0;
        for (Integer h : hs) {
            if(!hs.contains(h-1)){
                int num = h;
                int len = 1;
                while (hs.contains(num+1)){
                    len++;
                    num++;
                }
                maxLen = Math.max(len,maxLen);
            }
        }
        return maxLen;
    }
}
