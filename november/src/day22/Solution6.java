package day22;

import java.util.*;

/**
 * 给你一个整数数组 nums 和一个整数 k ，请你返回其中出现频率前 k 高的元素。你可以按 任意顺序 返回答案。
 *
 *
 *
 * 示例 1:
 *
 * 输入: nums = [1,1,1,2,2,3], k = 2
 * 输出: [1,2]
 * 示例 2:
 *
 * 输入: nums = [1], k = 1
 * 输出: [1]
 */
public class Solution6 {
    public int[] topKFrequent(int[] nums, int k) {
        Map<Integer,Integer> map = new HashMap<>();
        for (int num : nums) {
            map.put(num,map.getOrDefault(num,0)+1);
        }
        PriorityQueue<int[]> pq = new PriorityQueue<>(new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return o2[1]-o1[1];
            }
        });
        for (Map.Entry<Integer, Integer> integerIntegerEntry : map.entrySet()) {
            int num = integerIntegerEntry.getKey(),count = integerIntegerEntry.getValue();
            pq.add(new int[]{num,count});
        }
        int []ans = new int[k];
        for (int i = 0; i < k; i++) {
            ans[i]=pq.poll()[0];
        }
        return ans;
    }
}
