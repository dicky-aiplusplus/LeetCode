package day11;

public class Solution1 {
    public boolean halvesAreAlike(String s) {
        int len = s.length();
        return count(s,0,len/2)==count(s,len/2,len);
    }
    public int count(String s,int start,int end){
        int num=0;
        for(int i = start;i<end;i++){
            switch (s.charAt(i)){
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'A':
                case 'E':
                case 'I':
                case 'O':
                case 'U':
                    num++;
                    break;
                default:
                    break;
            }
        }
        return num;
    }
}
