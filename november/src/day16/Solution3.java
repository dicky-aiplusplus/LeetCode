package day16;

/**
 * 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
 *
 * 如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。
 *
 * 假设环境不允许存储 64 位整数（有符号或无符号）。
 *
 *
 * 示例 1：
 *
 * 输入：x = 123
 * 输出：321
 * 示例 2：
 *
 * 输入：x = -123
 * 输出：-321
 * 示例 3：
 *
 * 输入：x = 120
 * 输出：21
 * 示例 4：
 *
 * 输入：x = 0
 * 输出：0
 */
public class Solution3 {
    public int reverse(int x) {
        long maxMod = Long.valueOf(Integer.MAX_VALUE);
        long minMod = Long.valueOf(Integer.MIN_VALUE);
        String strNum = String.valueOf(x);
        StringBuilder str = new StringBuilder();
        char[] chars = strNum.toCharArray();
        if(chars[0]=='-'){
            str.append(chars[0]);
            for(int i = strNum.length()-1;i>0;i++){
                str.append(chars[i]);
            }
        }else {
            for(int i = strNum.length()-1;i>=0;i++){
                str.append(chars[i]);
            }
        }
        long Fnum = Long.parseLong(str.toString());
        if(Fnum>maxMod||Fnum<minMod){
            return 0;
        }
        return Math.toIntExact(Fnum);
    }
}
