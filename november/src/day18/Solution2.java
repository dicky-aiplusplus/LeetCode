package day18;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个二叉树的 根节点 root，想象自己站在它的右侧，按照从顶部到底部的顺序，返回从右侧所能看到的节点值。
 */
public class Solution2 {
    List<Integer> list = new ArrayList<>();
    int count = 0;
    public List<Integer> rightSideView(TreeNode root) {
        dfs(root,1);
        return list;
    }
    public void dfs(TreeNode root,int num){
        if(root==null){
            return;
        }
        if(num>count){
            count++;
            list.add(root.val);
        }
        dfs(root.right,num+1);
        dfs(root.left,num+1);
    }
}
