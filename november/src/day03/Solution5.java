package day03;

/**
 * 给定两个大小分别为 m 和 n 的正序（从小到大）数组nums1 和nums2。请你找出并返回这两个正序数组的 中位数 。
 *
 * 算法的时间复杂度应该为 O(log (m+n)) 。
 *
 *
 *
 * 示例 1：
 *
 * 输入：nums1 = [1,3], nums2 = [2]
 * 输出：2.00000
 * 解释：合并数组 = [1,2,3] ，中位数 2
 * 示例 2：
 *
 * 输入：nums1 = [1,2], nums2 = [3,4]
 * 输出：2.50000
 * 解释：合并数组 = [1,2,3,4] ，中位数 (2 + 3) / 2 = 2.5
 *
 *
 */
public class Solution5 {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int index1 = 0,index2=0,len1=nums1.length,len2=nums2.length;
        int []number=new int[len1+len2];
        for (int i = 0; i < number.length; i++) {
            if(index1<len1&&index2<len2&&nums1[index1]<nums2[index2]){
                number[i]=nums1[index1];
                index1++;
                continue;
            }
            if(index1<len1&&index2<len2&&nums1[index1]>=nums2[index2]){
                number[i]=nums2[index2];
                index2++;
                continue;
            }
            if(index1>=len1){
                number[i]=nums1[index2];
                index2++;
                continue;
            }
            if(index2>=len2){
                number[i]=nums1[index1];
                index1++;
                continue;
            }
        }
        if(number.length%2!=0){
            return (double)number[number.length/2];
        }
        return ((double)number[number.length/2]+(double)number[number.length/2-1])/2;
    }
}
