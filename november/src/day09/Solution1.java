package day09;

/**
 * 给定两个字符串形式的非负整数 num1 和num2 ，计算它们的和并同样以字符串形式返回。
 *
 * 你不能使用任何內建的用于处理大整数的库（比如 BigInteger）， 也不能直接将输入的字符串转换为整数形式。
 *
 *
 *
 * 示例 1：
 *
 * 输入：num1 = "11", num2 = "123"
 * 输出："134"
 * 示例 2：
 *
 * 输入：num1 = "456", num2 = "77"
 * 输出："533"
 * 示例 3：
 *
 * 输入：num1 = "0", num2 = "0"
 * 输出："0"
 */
public class Solution1 {
    public String addStrings(String num1, String num2) {
        if(num1.equals(num2)&&num1.equals("0")){
            return "0";
        }
        int n = num1.length()>num2.length()?num1.length():num2.length();
        int []ans = new int[n+1];
        int []nums1=new int[n];
        int index1 = n-1;
        for(int i = num1.length()-1;i>=0;i--){
            nums1[index1] = num1.charAt(i)-'0';
            index1--;
        }
        int []nums2 = new int[n];
        int index2 = n-1;
        for(int i = num2.length()-1;i>=0;i--){
            nums2[index2] = num2.charAt(i)-'0';
            index2--;
        }
        for (int i = n-1; i >=0; i--) {
            int num= nums1[i]+nums2[i]+ans[i+1];
            if(num>9){
                ans[i+1]=num-10;
                ans[i]=1;
            }else {
                ans[i+1]=num;
            }
        }
        StringBuilder str = new StringBuilder();
        boolean f = false;
        for (int i = 0; i < n + 1; i++) {
            if(ans[i]!=0){
                f=true;
            }
            if(f){
                str.append(ans[i]);
            }
        }
        return str.toString();
    }
}
